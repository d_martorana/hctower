﻿namespace CM18tool
{
    partial class HcTower
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HcTower));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelMain = new System.Windows.Forms.SplitContainer();
            this.panelLeft = new System.Windows.Forms.SplitContainer();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.lstComScope = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.grpSingleCommand = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTransparentByte = new System.Windows.Forms.TextBox();
            this.btnTransparentSend = new System.Windows.Forms.Button();
            this.btnTransparentExit = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTransparentAnswer = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtTransparentTimeO = new System.Windows.Forms.TextBox();
            this.txtTransparentCmd = new System.Windows.Forms.TextBox();
            this.grpSerialNumber = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCassHTL = new System.Windows.Forms.TextBox();
            this.txtCassGTL = new System.Windows.Forms.TextBox();
            this.txtTowerTL = new System.Windows.Forms.TextBox();
            this.txtCassHSN = new System.Windows.Forms.TextBox();
            this.txtCassGSN = new System.Windows.Forms.TextBox();
            this.txtTowerSN = new System.Windows.Forms.TextBox();
            this.txtMain = new System.Windows.Forms.TextBox();
            this.dgValues = new System.Windows.Forms.DataGridView();
            this.col1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnStart = new System.Windows.Forms.Button();
            this.lblMessage = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.connectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.singleTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.factoryCalibrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.photoCalibrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.presetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.safeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.drumAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.drumBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.drumCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.drumDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.drumEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.drumFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.drumGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.drumHToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.towerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.horizzontalTransportMotorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.middleTowerSorterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.middleTowerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cassetteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cassettePinchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.drumGToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.drumHToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cassetteTapeLenghtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.measureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unwindToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verticalTransportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.getModuleErrorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.singleCommandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.singleTestToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.guidedTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblConnection = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblTowerSN = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblCassGSN = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel6 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblCassHSN = new System.Windows.Forms.ToolStripStatusLabel();
            this.windGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windHToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unwindGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unwindHToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.measureGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.measureHToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelMain)).BeginInit();
            this.panelMain.Panel1.SuspendLayout();
            this.panelMain.Panel2.SuspendLayout();
            this.panelMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelLeft)).BeginInit();
            this.panelLeft.Panel1.SuspendLayout();
            this.panelLeft.Panel2.SuspendLayout();
            this.panelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.grpSingleCommand.SuspendLayout();
            this.grpSerialNumber.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgValues)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelMain
            // 
            this.panelMain.BackColor = System.Drawing.SystemColors.Control;
            this.panelMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(0, 24);
            this.panelMain.Name = "panelMain";
            // 
            // panelMain.Panel1
            // 
            this.panelMain.Panel1.Controls.Add(this.panelLeft);
            // 
            // panelMain.Panel2
            // 
            this.panelMain.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panelMain.Panel2.Controls.Add(this.grpSingleCommand);
            this.panelMain.Panel2.Controls.Add(this.grpSerialNumber);
            this.panelMain.Panel2.Controls.Add(this.txtMain);
            this.panelMain.Panel2.Controls.Add(this.dgValues);
            this.panelMain.Panel2.Controls.Add(this.btnStart);
            this.panelMain.Panel2.Controls.Add(this.lblMessage);
            this.panelMain.Panel2.Controls.Add(this.lblTitle);
            this.panelMain.Panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panelMain_Panel2_Paint);
            this.panelMain.Size = new System.Drawing.Size(968, 460);
            this.panelMain.SplitterDistance = 328;
            this.panelMain.TabIndex = 5;
            this.panelMain.KeyUp += new System.Windows.Forms.KeyEventHandler(this.HcTower_KeyUp);
            // 
            // panelLeft
            // 
            this.panelLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(0, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // panelLeft.Panel1
            // 
            this.panelLeft.Panel1.Controls.Add(this.picLogo);
            // 
            // panelLeft.Panel2
            // 
            this.panelLeft.Panel2.Controls.Add(this.lstComScope);
            this.panelLeft.Panel2.Controls.Add(this.label1);
            this.panelLeft.Size = new System.Drawing.Size(328, 460);
            this.panelLeft.SplitterDistance = 59;
            this.panelLeft.TabIndex = 0;
            this.panelLeft.KeyUp += new System.Windows.Forms.KeyEventHandler(this.HcTower_KeyUp);
            // 
            // picLogo
            // 
            this.picLogo.Image = ((System.Drawing.Image)(resources.GetObject("picLogo.Image")));
            this.picLogo.Location = new System.Drawing.Point(12, 10);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(43, 44);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLogo.TabIndex = 2;
            this.picLogo.TabStop = false;
            // 
            // lstComScope
            // 
            this.lstComScope.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstComScope.FormattingEnabled = true;
            this.lstComScope.Location = new System.Drawing.Point(6, 237);
            this.lstComScope.Name = "lstComScope";
            this.lstComScope.Size = new System.Drawing.Size(317, 134);
            this.lstComScope.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 18);
            this.label1.TabIndex = 3;
            this.label1.Text = "Test:";
            // 
            // grpSingleCommand
            // 
            this.grpSingleCommand.Controls.Add(this.label8);
            this.grpSingleCommand.Controls.Add(this.txtTransparentByte);
            this.grpSingleCommand.Controls.Add(this.btnTransparentSend);
            this.grpSingleCommand.Controls.Add(this.btnTransparentExit);
            this.grpSingleCommand.Controls.Add(this.label7);
            this.grpSingleCommand.Controls.Add(this.txtTransparentAnswer);
            this.grpSingleCommand.Controls.Add(this.label10);
            this.grpSingleCommand.Controls.Add(this.label11);
            this.grpSingleCommand.Controls.Add(this.txtTransparentTimeO);
            this.grpSingleCommand.Controls.Add(this.txtTransparentCmd);
            this.grpSingleCommand.Location = new System.Drawing.Point(133, 246);
            this.grpSingleCommand.Name = "grpSingleCommand";
            this.grpSingleCommand.Size = new System.Drawing.Size(339, 173);
            this.grpSingleCommand.TabIndex = 11;
            this.grpSingleCommand.TabStop = false;
            this.grpSingleCommand.Text = "SINGLE COMMAND";
            this.grpSingleCommand.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(172, 83);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "Ans Byte Num";
            // 
            // txtTransparentByte
            // 
            this.txtTransparentByte.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtTransparentByte.Location = new System.Drawing.Point(251, 76);
            this.txtTransparentByte.MaxLength = 2;
            this.txtTransparentByte.Name = "txtTransparentByte";
            this.txtTransparentByte.Size = new System.Drawing.Size(58, 24);
            this.txtTransparentByte.TabIndex = 21;
            this.txtTransparentByte.Text = "0";
            // 
            // btnTransparentSend
            // 
            this.btnTransparentSend.Location = new System.Drawing.Point(12, 140);
            this.btnTransparentSend.Name = "btnTransparentSend";
            this.btnTransparentSend.Size = new System.Drawing.Size(134, 23);
            this.btnTransparentSend.TabIndex = 20;
            this.btnTransparentSend.Text = "Send";
            this.btnTransparentSend.UseVisualStyleBackColor = true;
            this.btnTransparentSend.Click += new System.EventHandler(this.btnTransparentSend_Click);
            // 
            // btnTransparentExit
            // 
            this.btnTransparentExit.Location = new System.Drawing.Point(175, 140);
            this.btnTransparentExit.Name = "btnTransparentExit";
            this.btnTransparentExit.Size = new System.Drawing.Size(134, 23);
            this.btnTransparentExit.TabIndex = 19;
            this.btnTransparentExit.Text = "Exit";
            this.btnTransparentExit.UseVisualStyleBackColor = true;
            this.btnTransparentExit.Click += new System.EventHandler(this.btnTransparentExit_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 113);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Answer";
            // 
            // txtTransparentAnswer
            // 
            this.txtTransparentAnswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtTransparentAnswer.Location = new System.Drawing.Point(88, 106);
            this.txtTransparentAnswer.MaxLength = 2;
            this.txtTransparentAnswer.Name = "txtTransparentAnswer";
            this.txtTransparentAnswer.Size = new System.Drawing.Size(221, 24);
            this.txtTransparentAnswer.TabIndex = 17;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 53);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "Command:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 83);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(66, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "Time out mS";
            // 
            // txtTransparentTimeO
            // 
            this.txtTransparentTimeO.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtTransparentTimeO.Location = new System.Drawing.Point(88, 76);
            this.txtTransparentTimeO.MaxLength = 4;
            this.txtTransparentTimeO.Name = "txtTransparentTimeO";
            this.txtTransparentTimeO.Size = new System.Drawing.Size(58, 24);
            this.txtTransparentTimeO.TabIndex = 1;
            this.txtTransparentTimeO.Text = "1000";
            // 
            // txtTransparentCmd
            // 
            this.txtTransparentCmd.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtTransparentCmd.Location = new System.Drawing.Point(88, 46);
            this.txtTransparentCmd.MaxLength = 12;
            this.txtTransparentCmd.Name = "txtTransparentCmd";
            this.txtTransparentCmd.Size = new System.Drawing.Size(221, 24);
            this.txtTransparentCmd.TabIndex = 0;
            this.txtTransparentCmd.TextChanged += new System.EventHandler(this.txtTransparentCmd_TextChanged);
            // 
            // grpSerialNumber
            // 
            this.grpSerialNumber.Controls.Add(this.label3);
            this.grpSerialNumber.Controls.Add(this.label6);
            this.grpSerialNumber.Controls.Add(this.label5);
            this.grpSerialNumber.Controls.Add(this.label4);
            this.grpSerialNumber.Controls.Add(this.label2);
            this.grpSerialNumber.Controls.Add(this.txtCassHTL);
            this.grpSerialNumber.Controls.Add(this.txtCassGTL);
            this.grpSerialNumber.Controls.Add(this.txtTowerTL);
            this.grpSerialNumber.Controls.Add(this.txtCassHSN);
            this.grpSerialNumber.Controls.Add(this.txtCassGSN);
            this.grpSerialNumber.Controls.Add(this.txtTowerSN);
            this.grpSerialNumber.Location = new System.Drawing.Point(152, 263);
            this.grpSerialNumber.Name = "grpSerialNumber";
            this.grpSerialNumber.Size = new System.Drawing.Size(307, 126);
            this.grpSerialNumber.TabIndex = 10;
            this.grpSerialNumber.TabStop = false;
            this.grpSerialNumber.Text = "SERIAL NUMBER";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(208, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Technical Level";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "CASS H";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "CASS G";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "TOWER";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(60, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Serial Number";
            // 
            // txtCassHTL
            // 
            this.txtCassHTL.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtCassHTL.Location = new System.Drawing.Point(211, 93);
            this.txtCassHTL.MaxLength = 2;
            this.txtCassHTL.Name = "txtCassHTL";
            this.txtCassHTL.Size = new System.Drawing.Size(80, 24);
            this.txtCassHTL.TabIndex = 5;
            this.txtCassHTL.EnabledChanged += new System.EventHandler(this.txtTowerSN_EnabledChanged);
            this.txtCassHTL.Enter += new System.EventHandler(this.txtCassHTL_Enter);
            this.txtCassHTL.KeyUp += new System.Windows.Forms.KeyEventHandler(this.CassHTLCheck);
            // 
            // txtCassGTL
            // 
            this.txtCassGTL.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtCassGTL.Location = new System.Drawing.Point(211, 63);
            this.txtCassGTL.MaxLength = 2;
            this.txtCassGTL.Name = "txtCassGTL";
            this.txtCassGTL.Size = new System.Drawing.Size(80, 24);
            this.txtCassGTL.TabIndex = 3;
            this.txtCassGTL.EnabledChanged += new System.EventHandler(this.txtTowerSN_EnabledChanged);
            this.txtCassGTL.Enter += new System.EventHandler(this.txtCassGTL_Enter);
            this.txtCassGTL.KeyUp += new System.Windows.Forms.KeyEventHandler(this.CassGTLCheck);
            // 
            // txtTowerTL
            // 
            this.txtTowerTL.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtTowerTL.Location = new System.Drawing.Point(211, 33);
            this.txtTowerTL.MaxLength = 2;
            this.txtTowerTL.Name = "txtTowerTL";
            this.txtTowerTL.Size = new System.Drawing.Size(80, 24);
            this.txtTowerTL.TabIndex = 1;
            this.txtTowerTL.EnabledChanged += new System.EventHandler(this.txtTowerSN_EnabledChanged);
            this.txtTowerTL.Enter += new System.EventHandler(this.txtTowerTL_Enter);
            this.txtTowerTL.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TowerTLCheck);
            // 
            // txtCassHSN
            // 
            this.txtCassHSN.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtCassHSN.Location = new System.Drawing.Point(63, 93);
            this.txtCassHSN.MaxLength = 12;
            this.txtCassHSN.Name = "txtCassHSN";
            this.txtCassHSN.Size = new System.Drawing.Size(142, 24);
            this.txtCassHSN.TabIndex = 4;
            this.txtCassHSN.EnabledChanged += new System.EventHandler(this.txtTowerSN_EnabledChanged);
            this.txtCassHSN.Enter += new System.EventHandler(this.txtCassHSN_Enter);
            this.txtCassHSN.KeyUp += new System.Windows.Forms.KeyEventHandler(this.CassHSNCheck);
            // 
            // txtCassGSN
            // 
            this.txtCassGSN.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtCassGSN.Location = new System.Drawing.Point(63, 63);
            this.txtCassGSN.MaxLength = 12;
            this.txtCassGSN.Name = "txtCassGSN";
            this.txtCassGSN.Size = new System.Drawing.Size(142, 24);
            this.txtCassGSN.TabIndex = 2;
            this.txtCassGSN.EnabledChanged += new System.EventHandler(this.txtTowerSN_EnabledChanged);
            this.txtCassGSN.Enter += new System.EventHandler(this.txtCassGSN_Enter);
            this.txtCassGSN.KeyUp += new System.Windows.Forms.KeyEventHandler(this.CassGSNCheck);
            // 
            // txtTowerSN
            // 
            this.txtTowerSN.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtTowerSN.Location = new System.Drawing.Point(63, 33);
            this.txtTowerSN.MaxLength = 12;
            this.txtTowerSN.Name = "txtTowerSN";
            this.txtTowerSN.Size = new System.Drawing.Size(142, 24);
            this.txtTowerSN.TabIndex = 0;
            this.txtTowerSN.EnabledChanged += new System.EventHandler(this.txtTowerSN_EnabledChanged);
            this.txtTowerSN.Enter += new System.EventHandler(this.txtTowerSN_Enter);
            this.txtTowerSN.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TowerSNCheck);
            // 
            // txtMain
            // 
            this.txtMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtMain.Location = new System.Drawing.Point(17, 196);
            this.txtMain.MaxLength = 12;
            this.txtMain.Name = "txtMain";
            this.txtMain.Size = new System.Drawing.Size(554, 24);
            this.txtMain.TabIndex = 9;
            this.txtMain.Visible = false;
            this.txtMain.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtMain_KeyUp);
            // 
            // dgValues
            // 
            this.dgValues.AllowUserToAddRows = false;
            this.dgValues.AllowUserToDeleteRows = false;
            this.dgValues.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgValues.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgValues.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col1,
            this.col2,
            this.col3,
            this.col4});
            this.dgValues.Location = new System.Drawing.Point(18, 224);
            this.dgValues.Name = "dgValues";
            this.dgValues.ReadOnly = true;
            this.dgValues.Size = new System.Drawing.Size(553, 221);
            this.dgValues.TabIndex = 8;
            this.dgValues.Visible = false;
            this.dgValues.KeyUp += new System.Windows.Forms.KeyEventHandler(this.HcTower_KeyUp);
            // 
            // col1
            // 
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col1.DefaultCellStyle = dataGridViewCellStyle13;
            this.col1.HeaderText = "Name";
            this.col1.Name = "col1";
            this.col1.ReadOnly = true;
            // 
            // col2
            // 
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col2.DefaultCellStyle = dataGridViewCellStyle14;
            this.col2.HeaderText = "Value";
            this.col2.Name = "col2";
            this.col2.ReadOnly = true;
            // 
            // col3
            // 
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col3.DefaultCellStyle = dataGridViewCellStyle15;
            this.col3.HeaderText = "Range";
            this.col3.Name = "col3";
            this.col3.ReadOnly = true;
            // 
            // col4
            // 
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.col4.DefaultCellStyle = dataGridViewCellStyle16;
            this.col4.HeaderText = "Result";
            this.col4.Name = "col4";
            this.col4.ReadOnly = true;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(246, 130);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(118, 60);
            this.btnStart.TabIndex = 7;
            this.btnStart.Text = "START";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.TowerTest);
            this.btnStart.KeyUp += new System.Windows.Forms.KeyEventHandler(this.HcTower_KeyUp);
            // 
            // lblMessage
            // 
            this.lblMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessage.Location = new System.Drawing.Point(13, 45);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(581, 80);
            this.lblMessage.TabIndex = 6;
            this.lblMessage.Text = "lblMessage";
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(13, 10);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(581, 28);
            this.lblTitle.TabIndex = 5;
            this.lblTitle.Text = "lblTitle";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.connectionToolStripMenuItem,
            this.singleTestToolStripMenuItem,
            this.modeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(968, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // connectionToolStripMenuItem
            // 
            this.connectionToolStripMenuItem.Name = "connectionToolStripMenuItem";
            this.connectionToolStripMenuItem.Size = new System.Drawing.Size(81, 20);
            this.connectionToolStripMenuItem.Text = "Connection";
            // 
            // singleTestToolStripMenuItem
            // 
            this.singleTestToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.factoryCalibrationToolStripMenuItem,
            this.photoCalibrationToolStripMenuItem,
            this.presetToolStripMenuItem,
            this.statusToolStripMenuItem,
            this.horizzontalTransportMotorToolStripMenuItem,
            this.middleTowerSorterToolStripMenuItem,
            this.cassettePinchToolStripMenuItem,
            this.cassetteTapeLenghtToolStripMenuItem,
            this.verticalTransportToolStripMenuItem,
            this.toolStripSeparator1,
            this.getModuleErrorToolStripMenuItem,
            this.singleCommandToolStripMenuItem});
            this.singleTestToolStripMenuItem.Enabled = false;
            this.singleTestToolStripMenuItem.Name = "singleTestToolStripMenuItem";
            this.singleTestToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.singleTestToolStripMenuItem.Text = "Single Test";
            this.singleTestToolStripMenuItem.Click += new System.EventHandler(this.singleTestToolStripMenuItem_Click);
            // 
            // factoryCalibrationToolStripMenuItem
            // 
            this.factoryCalibrationToolStripMenuItem.Name = "factoryCalibrationToolStripMenuItem";
            this.factoryCalibrationToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.factoryCalibrationToolStripMenuItem.Text = "Factory Calibration";
            this.factoryCalibrationToolStripMenuItem.Click += new System.EventHandler(this.factoryCalibrationToolStripMenuItem_Click);
            // 
            // photoCalibrationToolStripMenuItem
            // 
            this.photoCalibrationToolStripMenuItem.Name = "photoCalibrationToolStripMenuItem";
            this.photoCalibrationToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.photoCalibrationToolStripMenuItem.Text = "Photo Calibration";
            this.photoCalibrationToolStripMenuItem.Click += new System.EventHandler(this.photoCalibrationToolStripMenuItem_Click);
            // 
            // presetToolStripMenuItem
            // 
            this.presetToolStripMenuItem.Name = "presetToolStripMenuItem";
            this.presetToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.presetToolStripMenuItem.Text = "Preset";
            this.presetToolStripMenuItem.Click += new System.EventHandler(this.presetToolStripMenuItem_Click);
            // 
            // statusToolStripMenuItem
            // 
            this.statusToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.safeToolStripMenuItem,
            this.drumAToolStripMenuItem,
            this.drumBToolStripMenuItem,
            this.drumCToolStripMenuItem,
            this.drumDToolStripMenuItem,
            this.drumEToolStripMenuItem,
            this.drumFToolStripMenuItem,
            this.drumGToolStripMenuItem,
            this.drumHToolStripMenuItem,
            this.towerToolStripMenuItem});
            this.statusToolStripMenuItem.Name = "statusToolStripMenuItem";
            this.statusToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.statusToolStripMenuItem.Text = "Status";
            // 
            // safeToolStripMenuItem
            // 
            this.safeToolStripMenuItem.Name = "safeToolStripMenuItem";
            this.safeToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.safeToolStripMenuItem.Text = "Safe";
            this.safeToolStripMenuItem.Click += new System.EventHandler(this.safeToolStripMenuItem_Click);
            // 
            // drumAToolStripMenuItem
            // 
            this.drumAToolStripMenuItem.Name = "drumAToolStripMenuItem";
            this.drumAToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.drumAToolStripMenuItem.Text = "Drum A";
            this.drumAToolStripMenuItem.Click += new System.EventHandler(this.drumAToolStripMenuItem_Click);
            // 
            // drumBToolStripMenuItem
            // 
            this.drumBToolStripMenuItem.Name = "drumBToolStripMenuItem";
            this.drumBToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.drumBToolStripMenuItem.Text = "Drum B";
            this.drumBToolStripMenuItem.Click += new System.EventHandler(this.drumBToolStripMenuItem_Click);
            // 
            // drumCToolStripMenuItem
            // 
            this.drumCToolStripMenuItem.Name = "drumCToolStripMenuItem";
            this.drumCToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.drumCToolStripMenuItem.Text = "Drum C";
            this.drumCToolStripMenuItem.Click += new System.EventHandler(this.drumCToolStripMenuItem_Click);
            // 
            // drumDToolStripMenuItem
            // 
            this.drumDToolStripMenuItem.Name = "drumDToolStripMenuItem";
            this.drumDToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.drumDToolStripMenuItem.Text = "Drum D";
            this.drumDToolStripMenuItem.Click += new System.EventHandler(this.drumDToolStripMenuItem_Click);
            // 
            // drumEToolStripMenuItem
            // 
            this.drumEToolStripMenuItem.Name = "drumEToolStripMenuItem";
            this.drumEToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.drumEToolStripMenuItem.Text = "Drum E";
            this.drumEToolStripMenuItem.Click += new System.EventHandler(this.drumEToolStripMenuItem_Click);
            // 
            // drumFToolStripMenuItem
            // 
            this.drumFToolStripMenuItem.Name = "drumFToolStripMenuItem";
            this.drumFToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.drumFToolStripMenuItem.Text = "Drum F";
            this.drumFToolStripMenuItem.Click += new System.EventHandler(this.drumFToolStripMenuItem_Click);
            // 
            // drumGToolStripMenuItem
            // 
            this.drumGToolStripMenuItem.Name = "drumGToolStripMenuItem";
            this.drumGToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.drumGToolStripMenuItem.Text = "Drum G";
            this.drumGToolStripMenuItem.Click += new System.EventHandler(this.drumGToolStripMenuItem_Click);
            // 
            // drumHToolStripMenuItem
            // 
            this.drumHToolStripMenuItem.Name = "drumHToolStripMenuItem";
            this.drumHToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.drumHToolStripMenuItem.Text = "Drum H";
            this.drumHToolStripMenuItem.Click += new System.EventHandler(this.drumHToolStripMenuItem_Click);
            // 
            // towerToolStripMenuItem
            // 
            this.towerToolStripMenuItem.Name = "towerToolStripMenuItem";
            this.towerToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.towerToolStripMenuItem.Text = "Tower";
            // 
            // horizzontalTransportMotorToolStripMenuItem
            // 
            this.horizzontalTransportMotorToolStripMenuItem.Name = "horizzontalTransportMotorToolStripMenuItem";
            this.horizzontalTransportMotorToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.horizzontalTransportMotorToolStripMenuItem.Text = "Horizzontal Transport Motor";
            this.horizzontalTransportMotorToolStripMenuItem.Click += new System.EventHandler(this.horizzontalTransportMotorToolStripMenuItem_Click);
            // 
            // middleTowerSorterToolStripMenuItem
            // 
            this.middleTowerSorterToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.middleTowerToolStripMenuItem,
            this.cassetteToolStripMenuItem});
            this.middleTowerSorterToolStripMenuItem.Name = "middleTowerSorterToolStripMenuItem";
            this.middleTowerSorterToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.middleTowerSorterToolStripMenuItem.Text = "Sorter";
            // 
            // middleTowerToolStripMenuItem
            // 
            this.middleTowerToolStripMenuItem.Name = "middleTowerToolStripMenuItem";
            this.middleTowerToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.middleTowerToolStripMenuItem.Text = "Middle Tower";
            this.middleTowerToolStripMenuItem.Click += new System.EventHandler(this.middleTowerToolStripMenuItem_Click);
            // 
            // cassetteToolStripMenuItem
            // 
            this.cassetteToolStripMenuItem.Name = "cassetteToolStripMenuItem";
            this.cassetteToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.cassetteToolStripMenuItem.Text = "Cassette";
            this.cassetteToolStripMenuItem.Click += new System.EventHandler(this.cassetteToolStripMenuItem_Click);
            // 
            // cassettePinchToolStripMenuItem
            // 
            this.cassettePinchToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.drumGToolStripMenuItem1,
            this.drumHToolStripMenuItem1});
            this.cassettePinchToolStripMenuItem.Name = "cassettePinchToolStripMenuItem";
            this.cassettePinchToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.cassettePinchToolStripMenuItem.Text = "Cassette Pinch";
            // 
            // drumGToolStripMenuItem1
            // 
            this.drumGToolStripMenuItem1.Name = "drumGToolStripMenuItem1";
            this.drumGToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.drumGToolStripMenuItem1.Text = "Drum G";
            this.drumGToolStripMenuItem1.Click += new System.EventHandler(this.drumGToolStripMenuItem1_Click);
            // 
            // drumHToolStripMenuItem1
            // 
            this.drumHToolStripMenuItem1.Name = "drumHToolStripMenuItem1";
            this.drumHToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.drumHToolStripMenuItem1.Text = "Drum H";
            this.drumHToolStripMenuItem1.Click += new System.EventHandler(this.drumHToolStripMenuItem1_Click);
            // 
            // cassetteTapeLenghtToolStripMenuItem
            // 
            this.cassetteTapeLenghtToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.measureToolStripMenuItem,
            this.windToolStripMenuItem,
            this.unwindToolStripMenuItem});
            this.cassetteTapeLenghtToolStripMenuItem.Name = "cassetteTapeLenghtToolStripMenuItem";
            this.cassetteTapeLenghtToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.cassetteTapeLenghtToolStripMenuItem.Text = "Cassette Tape";
            // 
            // measureToolStripMenuItem
            // 
            this.measureToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.measureGToolStripMenuItem,
            this.measureHToolStripMenuItem});
            this.measureToolStripMenuItem.Name = "measureToolStripMenuItem";
            this.measureToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.measureToolStripMenuItem.Text = "Measure";
            this.measureToolStripMenuItem.Click += new System.EventHandler(this.measureToolStripMenuItem_Click);
            // 
            // windToolStripMenuItem
            // 
            this.windToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.windGToolStripMenuItem,
            this.windHToolStripMenuItem});
            this.windToolStripMenuItem.Name = "windToolStripMenuItem";
            this.windToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.windToolStripMenuItem.Text = "Wind";
            this.windToolStripMenuItem.Click += new System.EventHandler(this.windToolStripMenuItem_Click);
            // 
            // unwindToolStripMenuItem
            // 
            this.unwindToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.unwindGToolStripMenuItem,
            this.unwindHToolStripMenuItem});
            this.unwindToolStripMenuItem.Name = "unwindToolStripMenuItem";
            this.unwindToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.unwindToolStripMenuItem.Text = "Unwind";
            this.unwindToolStripMenuItem.Click += new System.EventHandler(this.unwindToolStripMenuItem_Click);
            // 
            // verticalTransportToolStripMenuItem
            // 
            this.verticalTransportToolStripMenuItem.Name = "verticalTransportToolStripMenuItem";
            this.verticalTransportToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.verticalTransportToolStripMenuItem.Text = "Vertical Transport";
            this.verticalTransportToolStripMenuItem.Click += new System.EventHandler(this.verticalTransportToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(220, 6);
            // 
            // getModuleErrorToolStripMenuItem
            // 
            this.getModuleErrorToolStripMenuItem.Name = "getModuleErrorToolStripMenuItem";
            this.getModuleErrorToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.getModuleErrorToolStripMenuItem.Text = "Get Module Error";
            this.getModuleErrorToolStripMenuItem.Click += new System.EventHandler(this.getModuleErrorToolStripMenuItem_Click);
            // 
            // singleCommandToolStripMenuItem
            // 
            this.singleCommandToolStripMenuItem.Name = "singleCommandToolStripMenuItem";
            this.singleCommandToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.singleCommandToolStripMenuItem.Text = "Single Command";
            this.singleCommandToolStripMenuItem.Click += new System.EventHandler(this.singleCommandToolStripMenuItem_Click);
            // 
            // modeToolStripMenuItem
            // 
            this.modeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.singleTestToolStripMenuItem1,
            this.guidedTestToolStripMenuItem});
            this.modeToolStripMenuItem.Name = "modeToolStripMenuItem";
            this.modeToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.modeToolStripMenuItem.Text = "Mode";
            // 
            // singleTestToolStripMenuItem1
            // 
            this.singleTestToolStripMenuItem1.Name = "singleTestToolStripMenuItem1";
            this.singleTestToolStripMenuItem1.Size = new System.Drawing.Size(136, 22);
            this.singleTestToolStripMenuItem1.Text = "Single Test";
            this.singleTestToolStripMenuItem1.Click += new System.EventHandler(this.singleTestToolStripMenuItem1_Click);
            // 
            // guidedTestToolStripMenuItem
            // 
            this.guidedTestToolStripMenuItem.Checked = true;
            this.guidedTestToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.guidedTestToolStripMenuItem.Name = "guidedTestToolStripMenuItem";
            this.guidedTestToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.guidedTestToolStripMenuItem.Text = "Guided Test";
            this.guidedTestToolStripMenuItem.Click += new System.EventHandler(this.singleTestToolStripMenuItem1_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.lblConnection,
            this.toolStripStatusLabel2,
            this.lblTowerSN,
            this.toolStripStatusLabel4,
            this.lblCassGSN,
            this.toolStripStatusLabel6,
            this.lblCassHSN});
            this.statusStrip1.Location = new System.Drawing.Point(0, 462);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(968, 22);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(129, 17);
            this.toolStripStatusLabel1.Text = "Connection Parameter:";
            // 
            // lblConnection
            // 
            this.lblConnection.Name = "lblConnection";
            this.lblConnection.Size = new System.Drawing.Size(13, 17);
            this.lblConnection.Text = "  ";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(60, 17);
            this.toolStripStatusLabel2.Text = "Tower SN:";
            // 
            // lblTowerSN
            // 
            this.lblTowerSN.Name = "lblTowerSN";
            this.lblTowerSN.Size = new System.Drawing.Size(12, 17);
            this.lblTowerSN.Text = "-";
            // 
            // toolStripStatusLabel4
            // 
            this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            this.toolStripStatusLabel4.Size = new System.Drawing.Size(83, 17);
            this.toolStripStatusLabel4.Text = "Cassette G SN:";
            // 
            // lblCassGSN
            // 
            this.lblCassGSN.Name = "lblCassGSN";
            this.lblCassGSN.Size = new System.Drawing.Size(12, 17);
            this.lblCassGSN.Text = "-";
            // 
            // toolStripStatusLabel6
            // 
            this.toolStripStatusLabel6.Name = "toolStripStatusLabel6";
            this.toolStripStatusLabel6.Size = new System.Drawing.Size(84, 17);
            this.toolStripStatusLabel6.Text = "Cassette H SN:";
            // 
            // lblCassHSN
            // 
            this.lblCassHSN.Name = "lblCassHSN";
            this.lblCassHSN.Size = new System.Drawing.Size(12, 17);
            this.lblCassHSN.Text = "-";
            // 
            // windGToolStripMenuItem
            // 
            this.windGToolStripMenuItem.Name = "windGToolStripMenuItem";
            this.windGToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.windGToolStripMenuItem.Text = "Wind G";
            this.windGToolStripMenuItem.Click += new System.EventHandler(this.windGToolStripMenuItem_Click);
            // 
            // windHToolStripMenuItem
            // 
            this.windHToolStripMenuItem.Name = "windHToolStripMenuItem";
            this.windHToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.windHToolStripMenuItem.Text = "Wind H";
            this.windHToolStripMenuItem.Click += new System.EventHandler(this.windHToolStripMenuItem_Click);
            // 
            // unwindGToolStripMenuItem
            // 
            this.unwindGToolStripMenuItem.Name = "unwindGToolStripMenuItem";
            this.unwindGToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.unwindGToolStripMenuItem.Text = "Unwind G";
            this.unwindGToolStripMenuItem.Click += new System.EventHandler(this.unwindGToolStripMenuItem_Click);
            // 
            // unwindHToolStripMenuItem
            // 
            this.unwindHToolStripMenuItem.Name = "unwindHToolStripMenuItem";
            this.unwindHToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.unwindHToolStripMenuItem.Text = "Unwind H";
            this.unwindHToolStripMenuItem.Click += new System.EventHandler(this.unwindHToolStripMenuItem_Click);
            // 
            // measureGToolStripMenuItem
            // 
            this.measureGToolStripMenuItem.Name = "measureGToolStripMenuItem";
            this.measureGToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.measureGToolStripMenuItem.Text = "Measure G";
            // 
            // measureHToolStripMenuItem
            // 
            this.measureHToolStripMenuItem.Name = "measureHToolStripMenuItem";
            this.measureHToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.measureHToolStripMenuItem.Text = "Measure H";
            // 
            // HcTower
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(968, 484);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panelMain);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "HcTower";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HcTower";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.HcTower_FormClosed);
            this.Load += new System.EventHandler(this.HcTower_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.HcTower_KeyUp);
            this.panelMain.Panel1.ResumeLayout(false);
            this.panelMain.Panel2.ResumeLayout(false);
            this.panelMain.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelMain)).EndInit();
            this.panelMain.ResumeLayout(false);
            this.panelLeft.Panel1.ResumeLayout(false);
            this.panelLeft.Panel2.ResumeLayout(false);
            this.panelLeft.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelLeft)).EndInit();
            this.panelLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.grpSingleCommand.ResumeLayout(false);
            this.grpSingleCommand.PerformLayout();
            this.grpSerialNumber.ResumeLayout(false);
            this.grpSerialNumber.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgValues)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.SplitContainer panelMain;
        private System.Windows.Forms.SplitContainer panelLeft;
        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.DataGridViewTextBoxColumn col1;
        private System.Windows.Forms.DataGridViewTextBoxColumn col2;
        private System.Windows.Forms.DataGridViewTextBoxColumn col3;
        private System.Windows.Forms.DataGridViewTextBoxColumn col4;
        public System.Windows.Forms.DataGridView dgValues;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem connectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem singleTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem factoryCalibrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem photoCalibrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem presetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem safeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem drumAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem drumBToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem drumCToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem drumDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem drumEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem drumFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem drumGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem drumHToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem towerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem horizzontalTransportMotorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem middleTowerSorterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem middleTowerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cassetteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cassetteTapeLenghtToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verticalTransportToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem getModuleErrorToolStripMenuItem;
        private System.Windows.Forms.TextBox txtMain;
        private System.Windows.Forms.ToolStripMenuItem modeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem singleTestToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem guidedTestToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel lblConnection;
        private System.Windows.Forms.ListBox lstComScope;
        private System.Windows.Forms.GroupBox grpSerialNumber;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCassHTL;
        private System.Windows.Forms.TextBox txtCassGTL;
        private System.Windows.Forms.TextBox txtTowerTL;
        private System.Windows.Forms.TextBox txtCassHSN;
        private System.Windows.Forms.TextBox txtCassGSN;
        private System.Windows.Forms.TextBox txtTowerSN;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel lblTowerSN;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
        private System.Windows.Forms.ToolStripStatusLabel lblCassGSN;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel6;
        private System.Windows.Forms.ToolStripStatusLabel lblCassHSN;
        private System.Windows.Forms.ToolStripMenuItem cassettePinchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem drumGToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem drumHToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem measureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unwindToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem singleCommandToolStripMenuItem;
        private System.Windows.Forms.GroupBox grpSingleCommand;
        private System.Windows.Forms.Button btnTransparentSend;
        private System.Windows.Forms.Button btnTransparentExit;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTransparentAnswer;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtTransparentTimeO;
        private System.Windows.Forms.TextBox txtTransparentCmd;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtTransparentByte;
        private System.Windows.Forms.ToolStripMenuItem measureGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem measureHToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windHToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unwindGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unwindHToolStripMenuItem;
    }
}