﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;
using System.Threading;
using CM18tool;

namespace CM18tool
{
    public partial class HcTower : Form
    {
        public static string language;
        public static string langFile;
        public static IniFile msgFile;
        Label[] lblTest = new Label[20];
        public static string[] testTitle = new string[20];
        public static string lastCommand;
        public static int commandReply;
        public static int testNum;
        //public SerialPort mySerial;
        public RS232 myRs232 = new RS232();
        public IniFile iniStatus = new IniFile("HCTower.status");
        public IniFile iniConfig = new IniFile("CM18HcTowerCfg.ini");
        public IniFile iniError = new IniFile("HCTower.error");
        public string status = "";
        public Arca.SHCLow myHcLow = new Arca.SHCLow();
        public string fileLogName = "HCTower.log";
        public string SrvFileLogFolder = "";
        public HcTower()//CmDLLs.ConnectionParam connectionParam
        {
            InitializeComponent();
            //lblConnection.Text = connectionParam.pRsConf.device + " ";
            //lblConnection.Text += connectionParam.pRsConf.baudrate + " ";
            InitializeAdditionalComponents();
        }

        public void InitializeAdditionalComponents()
        {
            this.Text = "HC Tower Test v" + Application.ProductVersion;
            myRs232.DataSent += new EventHandler(ComDataSent);
            myRs232.DataReceived += new EventHandler(ComDataReceived);
        }
        public void ComDataSent(object sender, EventArgs e)
        {
            lstComScope.ForeColor = Color.Black;
            lstComScope.Items.Add("SND:" + sender.ToString());
            lstComScope.SetSelected(lstComScope.Items.Count - 1, true);
        }

        public void ComDataReceived(object sender, EventArgs e)
        {
            lstComScope.Items.Add("RCV:" + sender.ToString());
            lstComScope.SetSelected(lstComScope.Items.Count - 1, true);
        }

        private void InitializeMyTower()
        {
            Array.Resize(ref myHcLow.tower, 4);
            Array.Resize(ref myHcLow.tower[0].drum, 2);
            Array.Resize(ref myHcLow.tower[1].drum, 2);
            Array.Resize(ref myHcLow.tower[2].drum, 2);
            Array.Resize(ref myHcLow.tower[3].drum, 2);

            myHcLow.tower[0].idTower = 0;
            myHcLow.tower[0].drum[0].address = "4";
            myHcLow.tower[0].drum[0].name = "A";
            myHcLow.tower[0].drum[0].status = "";
            myHcLow.tower[0].drum[1].address = "5";
            myHcLow.tower[0].drum[1].name = "B";
            myHcLow.tower[0].drum[1].status = "";

            myHcLow.tower[1].idTower = 1;
            myHcLow.tower[1].drum[0].address = "6";
            myHcLow.tower[1].drum[0].name = "C";
            myHcLow.tower[1].drum[0].status = "";
            myHcLow.tower[1].drum[1].address = "7";
            myHcLow.tower[1].drum[1].name = "D";
            myHcLow.tower[1].drum[1].status = "";

            myHcLow.tower[2].idTower = 2;
            myHcLow.tower[2].drum[0].address = "8";
            myHcLow.tower[2].drum[0].name = "E";
            myHcLow.tower[2].drum[0].status = "";
            myHcLow.tower[2].drum[1].address = "9";
            myHcLow.tower[2].drum[1].name = "F";
            myHcLow.tower[2].drum[1].status = "";

            myHcLow.tower[3].idTower = 3;
            myHcLow.tower[3].drum[0].address = "A";
            myHcLow.tower[3].drum[0].name = "G";
            myHcLow.tower[3].drum[0].status = "";
            myHcLow.tower[3].drum[1].address = "B";
            myHcLow.tower[3].drum[1].name = "H";
            myHcLow.tower[3].drum[1].status = "";

            for (int i=0;i<=3;i++)
            {
                myHcLow.tower[i].phTr1.idPar = "50";
                myHcLow.tower[i].phTr1.name = "phTr1";
            }
            
            for (int i = 0; i<4; i++)
            {
                for (int x = 0; x<2; x++)
                {
                    myHcLow.tower[i].drum[x].phIn.idPar = "30";
                    myHcLow.tower[i].drum[x].phIn.name = "phIn";
                    myHcLow.tower[i].drum[x].phEmpty.idPar = "31";
                    myHcLow.tower[i].drum[x].phEmpty.name = "phEmpty";
                    myHcLow.tower[i].drum[x].phFull.idPar = "32";
                    myHcLow.tower[i].drum[x].phFull.name = "phFull";
                    myHcLow.tower[i].drum[x].phFill.idPar = "33";
                    myHcLow.tower[i].drum[x].phFill.name = "phFill";
                }
            }
        }

        

        private void HcTower_Load(object sender, EventArgs e)
        {
            language = iniConfig.GetValue("option", "language");
            if (language == "")
            {
                MessageBox.Show("Wrong Language file setting");
            }
            langFile = iniConfig.GetValue("langFile", language);

            if (File.Exists(langFile) == false)
            {
                MessageBox.Show("Language file doesn't exist");
            }

            msgFile = new IniFile(langFile);
            CleanPanel();
            Arca.iniMessage = msgFile;
            IniFile hcTest = new IniFile("CM18HcTower.test");
            
            string value;
            testNum = 0;
            value = "";
            do
            {
                testNum += 1;
                value = hcTest.GetValue("Test" + testNum, "title");
            } while (value != "" & value !="Failed");

            Array.Resize(ref lblTest, testNum);
            Array.Resize(ref testTitle , testNum);

            InitializeMyTower();
            for (int i = 1;i<testNum;i++)
            {
                lblTest[i] = new Label();
                lblTest[i].ForeColor = Color.Red;
                lblTest[i].Font = new Font("Comics", 9, FontStyle.Bold);
                lblTest[i].AutoSize = true;
                lblTest[i].Height = 18;
                lblTest[i].Location =new System.Drawing.Point(3, 1 + i * 18);
                lblTest[i].Parent = panelLeft.Panel2;
                lblTest[i].Text = hcTest.GetValue("Test" + i, "title").ToUpper();
                lblTest[i].Visible = true;
                testTitle[i]= hcTest.GetValue("Test" + i, "tag").ToUpper();
            }
            CmDLLs.ConnPar.pRsConf.device = iniConfig.GetValue("DirectConnection", "Port");
            CmDLLs.ConnPar.pRsConf.baudrate = Convert.ToInt32(iniConfig.GetValue("DirectConnection", "Baudrate"));

            myRs232.ConfigCom(CmDLLs.ConnPar.pRsConf.device, CmDLLs.ConnPar.pRsConf.baudrate);

            btnStart.Enabled = false;
            SrvFileLogFolder = iniConfig.GetValue("option", "ServerLogFolder");
            Arca.SendLog(fileLogName,SrvFileLogFolder);
            this.ActiveControl = txtTowerSN;
        }

        void TextBoxErase(TextBox txtName)
        {
            txtName.Text = "";
            txtName.Enabled = true;
            txtName.Focus();
        }
        private void CleanPanel()
        {
            lblTitle.Text = msgFile.GetValue("messages", "msg1");
            lblMessage.Text = msgFile.GetValue("messages","msg27");
            txtMain.Enabled = false;
            txtMain.Text = "";
            grpSerialNumber.Enabled = true;
            grpSerialNumber.Visible = true;
            TextBoxErase(txtTowerSN);
            TextBoxErase(txtTowerTL);
            TextBoxErase(txtCassGSN);
            TextBoxErase(txtCassGTL);
            TextBoxErase(txtCassHSN);
            TextBoxErase(txtCassHTL);
            txtTowerSN.Focus();
            dgValues.Rows.Clear();
            dgValues.Visible = false;
            this.Text = "HC TOWER TEST";
            btnStart.Visible = true;
            btnStart.Enabled = false;
            
        }
        private void HcTower_FormClosed(object sender, FormClosedEventArgs e)
        {
            myRs232.CloseCom();
            Application.Exit();
        }


        private void button1_Click(object sender, EventArgs e)
        {

            //btnStar
        }

        string TLSave()
        {
            string key = "";
            string command = "";
            Arca.WriteMessage(lblMessage, 49); //Scrittura SN Tower in corso.
            myRs232.OpenCom();

        snTower:
            //Write Serial number tower
            command = "914003" + Arca.Ascii2Hex(myHcLow.tower[3].serialNumber); 
            myRs232.SendCommand(command, 1, 1000);
            //Read tower serial number
            command = "E15403";
            myRs232.SendCommand(command, 12, 1000);
            //Verify written Tower SN
            if (myHcLow.tower[3].serialNumber != Arca.Hex2Ascii(myRs232.commandAnswer))
            {
                //SN diverso
                Arca.WriteMessage(lblMessage, 523, Arca.errorColor, Arca.Hex2Ascii(myRs232.commandAnswer), myHcLow.tower[3].serialNumber); //Il SN della tower non è stato scritto correttamente, memorizzato {0} invece di {1}. Premere un tasto per ripetere, ESC per uscire
                key = Arca.WaitingKey();
                if (key == "ESC")
                {
                    goto error;
                }
                goto snTower;
            }

        TLTower:
            string fileTower = Application.StartupPath + "\\" + myHcLow.tower[3].serialNumber.Substring(0, 5) + "-" + myHcLow.tower[3].technicalLevel + ".ini";
            IniFile iniTower = new IniFile(fileTower);
            myHcLow.tower[3].mechanicalLevel = Arca.Ascii2Hex(iniTower.GetValue("Config", "MechanicalLevel").PadLeft(4,'0'));
            command = "914203" + myHcLow.tower[3].mechanicalLevel;
            myRs232.SendCommand(command, 1, 100);
            //analizzare stato safe
            command = "D177";
            myRs232.SendCommand(command, 0, 500);
            myRs232.SendCommand("F1", 1, 100);
            while (myRs232.commandAnswer == "01")
            {
                myRs232.SendCommand("F1", 1, 100);
                Arca.WaitingTime(100);
            }

            myRs232.SendCommand("E15703", 4, 100);
            Arca.LogWrite(fileLogName, ",053" + Arca.Hex2Ascii(myRs232.commandAnswer)); //Tower Mechanical Level
            if (myHcLow.tower[3].mechanicalLevel != Arca.Hex2Ascii(myRs232.commandAnswer))
            {
                //Mechanical level non impostato correttamente
                Arca.WriteMessage(lblMessage, 529, Arca.errorColor, Arca.Hex2Ascii(myRs232.commandAnswer), myHcLow.tower[3].mechanicalLevel); //Il MECHANICAL LEVEL della torre non è stato impostato correttamente,memorizzato {0} invece di {1}. Premere un tasto per ripetere, ESC per uscire
                key = Arca.WaitingKey();
                if (key == "ESC")
                {
                    goto error;
                }
                goto TLTower;
            }
        SNCassG:
            Arca.WriteMessage(lblMessage, 57,Arca.messageColor,"G"); //Scrittura SN Cassetto G in corso.
            //Write higher cassette Serial number (G)
            command = "9A3132333435361D" + Arca.Ascii2Hex(myHcLow.tower[3].drum[0].serialNumber.Substring(0, 4));
            myRs232.SendCommand(command, 0, 100);
            command = "9A3132333435361E" + Arca.Ascii2Hex(myHcLow.tower[3].drum[0].serialNumber.Substring(4, 4));
            myRs232.SendCommand(command, 0, 100);
            command = "9A3132333435361F" + Arca.Ascii2Hex(myHcLow.tower[3].drum[0].serialNumber.Substring(8, 4));
            myRs232.SendCommand(command, 0, 100);
            //Read higher cassette Serial number (G) 
            command = "EA1D";
            myRs232.SendCommand(command, 4, 200);
            myHcLow.tower[3].drum[0].snHigh = Arca.Hex2Ascii(myRs232.commandAnswer);
            command = "EA1E";
            myRs232.SendCommand(command, 4, 200);
            myHcLow.tower[3].drum[0].snMid = Arca.Hex2Ascii(myRs232.commandAnswer);
            command = "EA1F";
            myRs232.SendCommand(command, 4, 200);
            myHcLow.tower[3].drum[0].snLow = Arca.Hex2Ascii(myRs232.commandAnswer);
            //Verify written serial number
            if (myHcLow.tower[3].drum[0].serialNumber != myHcLow.tower[3].drum[0].snHigh + myHcLow.tower[3].drum[0].snMid + myHcLow.tower[3].drum[0].snLow)
            {
                //wrong sn read
                Arca.WriteMessage(lblMessage, 522, Arca.errorColor, "G", myHcLow.tower[3].drum[0].snHigh +
                    myHcLow.tower[3].drum[0].snMid + myHcLow.tower[3].drum[0].snLow, myHcLow.tower[3].drum[0].serialNumber); //Il SN dela cassetto {0} non è stato scritto correttamente, memorizzato {1} invece di {2}. Premere un tasto per ripetere, ESC per uscire
                key = Arca.WaitingKey();
                if (key == "ESC")
                {
                    goto error;
                }
                goto SNCassG;
            }

        TLCassG:
            //Write higher cassette TL  (G)
            string fileG = Application.StartupPath + "\\" + myHcLow.tower[3].drum[0].serialNumber.Substring(0,5) + "-" + myHcLow.tower[3].drum[0].technicalLevel + ".ini";
            if (File.Exists(fileG)!=true)
            {
                Arca.WriteMessage(lblMessage, 527, Arca.errorColor, myHcLow.tower[3].drum[0].serialNumber.Substring(0,5) + "-" + myHcLow.tower[3].drum[0].technicalLevel); //Non e' stato possibile trovare il file di configurazione per il cassetto {0}
                Arca.WaitingKey();
                goto error;
            }
            IniFile iniG = new IniFile(fileG);
            Arca.WriteMessage(lblMessage, 58, Arca.messageColor, "G"); //Scrittura TL Cassetto G in corso.
            myHcLow.tower[3].drum[0].mechanicalLevel = Arca.Ascii2Hex(iniG.GetValue("Config", "MechanicalLevel").PadLeft(4,'0'));
            command = "9A31323334353621" + myHcLow.tower[3].drum[0].mechanicalLevel ;
            
            myRs232.SendCommand(command, 0, 500);
            command = "D177";
            myRs232.SendCommand(command, 0, 500);
            myRs232.SendCommand("F1", 1, 100);
            while (myRs232.commandAnswer == "01")
            {
                myRs232.SendCommand("F1", 1, 100);
                Arca.WaitingTime(100);
            }
            //Read higher cassette TL  (G)
            command = "EA37";
            myRs232.SendCommand(command, 4, 1000);
            Arca.LogWrite(fileLogName, ",041" + Arca.Hex2Ascii(myRs232.commandAnswer)); //Cassette G Mechanical Level
            //Verify written higher cassette TL (G)
            if (myHcLow.tower[3].drum[0].mechanicalLevel !=  myRs232.commandAnswer)
            {
                //TL diverso
                Arca.WriteMessage(lblMessage, 524, Arca.errorColor, "G", Arca.Hex2Ascii(myRs232.commandAnswer),myHcLow.tower[3].drum[0].mechanicalLevel);//Il TL dela cassetto {0} non è stato scritto correttamente, memorizzato {1} invece di {2}. Premere un tasto per ripetere, ESC per uscire
                key = Arca.WaitingKey();
                if (key == "ESC")
                {
                    goto error;
                }
                goto TLCassG;
            }


        SNCassH:
            Arca.WriteMessage(lblMessage, 57, Arca.messageColor, "H"); //Scrittura SN Cassetto H in corso.
            ////Write lower cassette Serial number  (H)
            command = "9B3132333435361D" + Arca.Ascii2Hex(myHcLow.tower[3].drum[1].serialNumber.Substring(0, 4));
            myRs232.SendCommand(command, 0, 200);
            command = "9B3132333435361E" + Arca.Ascii2Hex(myHcLow.tower[3].drum[1].serialNumber.Substring(4, 4));
            myRs232.SendCommand(command, 0, 200);
            command = "9B3132333435361F" + Arca.Ascii2Hex(myHcLow.tower[3].drum[1].serialNumber.Substring(8, 4));
            myRs232.SendCommand(command, 0, 200);
            //Read lower cassette Serial number (H)
            command = "EB1D";
            myRs232.SendCommand(command, 4, 200);
            myHcLow.tower[3].drum[1].snHigh = Arca.Hex2Ascii(myRs232.commandAnswer);
            command = "EB1E";
            myRs232.SendCommand(command, 4, 200);
            myHcLow.tower[3].drum[1].snMid = Arca.Hex2Ascii(myRs232.commandAnswer);
            command = "EB1F";
            myRs232.SendCommand(command, 4, 200);
            myHcLow.tower[3].drum[1].snLow = Arca.Hex2Ascii(myRs232.commandAnswer);
            //Verify written serial number
            if (myHcLow.tower[3].drum[1].serialNumber != myHcLow.tower[3].drum[1].snHigh + myHcLow.tower[3].drum[1].snMid + myHcLow.tower[3].drum[1].snLow)
            {
                //matricola non scritta correttamente
                Arca.WriteMessage(lblMessage, 522, Arca.errorColor, "H", myHcLow.tower[3].drum[1].snHigh +
                    myHcLow.tower[3].drum[1].snMid + myHcLow.tower[3].drum[1].snLow, myHcLow.tower[3].drum[1].serialNumber); //Il SN dela cassetto {0} non è stato scritto correttamente, memorizzato {1} invece di {2}. Premere un tasto per ripetere, ESC per uscire
                key = Arca.WaitingKey();
                if (key == "ESC")
                {
                    goto error;
                }
                goto SNCassH;
            }

        TLCassH:
            //Write lower cassette TL  (H)
            string fileH = Application.StartupPath + "\\" + myHcLow.tower[3].drum[1].serialNumber.Substring(0, 5) + "-" + myHcLow.tower[3].drum[1].technicalLevel + ".ini";
            if (File.Exists(fileG) != true)
            {
                Arca.WriteMessage(lblMessage, 527, Arca.errorColor, myHcLow.tower[3].drum[1].serialNumber.Substring(0, 5) + "-" + myHcLow.tower[3].drum[1].technicalLevel); //Non e' stato possibile trovare il file di configurazione per il cassetto {0}
                Arca.WaitingKey();
                goto error;
            }
            IniFile iniH = new IniFile(fileH);
            Arca.WriteMessage(lblMessage, 58, Arca.messageColor, "H"); //Scrittura TL Cassetto H in corso.
            myHcLow.tower[3].drum[1].mechanicalLevel = Arca.Ascii2Hex(iniG.GetValue("Config", "MechanicalLevel").PadLeft(4, '0'));
            command = "9B31323334353621" + myHcLow.tower[3].drum[1].mechanicalLevel;
            myRs232.SendCommand(command, 0, 500);
            command = "D177";
            myRs232.SendCommand(command, 0, 500);
            myRs232.SendCommand("F1", 1, 100);
            while (myRs232.commandAnswer == "01")
            {
                myRs232.SendCommand("F1", 1, 100);
                Arca.WaitingTime(100);
            }
            //Read lower cassette TL  (H)
            command = "EB37";
            myRs232.SendCommand(command, 4, 100);
            Arca.LogWrite(fileLogName, ",043" + Arca.Hex2Ascii(myRs232.commandAnswer)); //Cassette H Mechanical Level
            //Verify written lower cassette TL (H)
            if (myHcLow.tower[3].drum[1].mechanicalLevel != myRs232.commandAnswer)
            {
                //TL diverso
                Arca.WriteMessage(lblMessage, 524, Arca.errorColor, "G", Arca.Hex2Ascii(myRs232.commandAnswer.Substring(4)), myHcLow.tower[3].drum[1].mechanicalLevel);//Il TL dela cassetto {0} non è stato scritto correttamente, memorizzato {1} invece di {2}. Premere un tasto per ripetere, ESC per uscire
                key = Arca.WaitingKey();
                if (key == "ESC")
                {
                    goto error;
                }
                goto TLCassH;
            }

            myRs232.CloseCom();
            return "OK";
        error:
            return "KO";
        }

        string PinchTest(string address, bool singleTest = false)
        {
            string key = "";
            int peakIntensity, peakTime, peakTimeMin, peakTimeMax;
            peakTimeMin =Convert.ToInt16(iniConfig.GetValue("drumTimeRange", "pinchTimeMin"));
            peakTimeMax = Convert.ToInt16(iniConfig.GetValue("drumTimeRange", "pinchTimeMax"));
            Arca.WriteMessage(lblMessage, 50); //Test Pinch in corso
            myRs232.OpenCom();

            intensity:
            myRs232.SendCommand("D" + address + "21",0, 1000);
            myRs232.SendCommand("E" + address + "35", 4, 100);
            if (myRs232.commandAnswer=="NO ANSWER") { goto end; }
            peakIntensity = Convert.ToInt16(Arca.Hex2Ascii(myRs232.commandAnswer));
            if (singleTest == false)
            {
                switch (address)
                {
                    case "A":
                        Arca.LogWrite(fileLogName, ",444" + peakIntensity.ToString());
                        break;
                    case "B":
                        Arca.LogWrite(fileLogName, ",446" + peakIntensity.ToString());
                        break;
                }
            }
            
            if (peakIntensity <= 150)
            {
                //error
                Arca.WriteMessage(lblMessage, 525, Arca.errorColor, (char)(Convert.ToInt16(Arca.Ascii2Hex(address)) + 30));//Picco intensità pinch roller cassetto {0} fuori dall'intervallo consentito. Premi un tasto per ripetere, ESC per uscire
                key = Arca.WaitingKey();
                if (key=="ESC")
                {
                    goto end;
                }
                goto intensity;
            }

            time:
            myRs232.SendCommand("E" + address + "36", 4, 100);
            peakTime = Convert.ToInt16(Arca.Hex2Ascii(myRs232.commandAnswer));
            if (singleTest == false)
            {
                switch (address)
                {
                    case "A":
                        Arca.LogWrite(fileLogName, ",445" + peakTime.ToString());
                        break;
                    case "B":
                        Arca.LogWrite(fileLogName, ",447" + peakTime.ToString());
                        break;
                }
            }
            if (peakTime < peakTimeMin | peakTime > peakTimeMax)
            {
                //error
                Arca.WriteMessage(lblMessage, 526, Arca.errorColor, (char)(Convert.ToInt16(Arca.Ascii2Hex(address)) + 30));//Durata picco pinch roller cassetto {0} fuori dall'intervallo consentito. Premere un tasto per ripetere, ESC per uscire
                key = Arca.WaitingKey();
                if (key == "ESC")
                {
                    goto end;
                }
                goto intensity;
            }

            myRs232.CloseCom();
            return "OK";
        end:
            myRs232.CloseCom();
            
            return "KO";
        }
        string FactoryCalibration(bool singleTest = false)
        {
            start:
            //D164 1byte   5sec    Factory calibration
            Arca.WriteMessage(lblMessage, 12); //Factory calibration in corso...
            Arca.WriteMessage(lblTitle, 2); //FACTORY CALIBRATION
            string command = "D164";
            int timeOut = 5000;
            myRs232.OpenCom();
            myRs232.SendCommand(command, 1, timeOut);
            myRs232.CloseCom();
            status = myRs232.commandAnswer;
            if (singleTest == false) { Arca.LogWrite(fileLogName, ",404" + status); } //HCTower Factory Calibration result
            switch (status)
            {
                case "40":
                    Arca.WriteMessage( lblMessage, lblMessage.Text + " " + status + " - " + iniStatus.GetValue("SafeStatus", status));
                    //Arca.WaitingTime(1000);
                    foreach (Control myLabel in panelLeft.Panel2.Controls)
                    {
                        if (myLabel.Text == "Factory calibration")
                        {
                            myLabel.ForeColor = Arca.correctColor;
                            break;
                        }
                    }
                    if (singleTest == false)
                    {
                        //CleanPanel();
                    }
                    return "OK";
                case "68":
                case "69":
                    Arca.WriteMessage(lblMessage, 510, Arca.errorColor, status, iniStatus.GetValue("SafeStatus", status)); //FALLITO! Stato {0} - {1}. Verificare il corretto posizionamento della bandella nera. Premere un tasto per ripetere, ESC per uscire
                    if (Arca.WaitingKey()=="ESC")
                    {
                        return status;
                    }
                    goto start;
                default:
                    Arca.WriteMessage(lblMessage,501,Arca.messageColor,status + " - " + iniStatus.GetValue("SafeStatus",status)); //FALLITO! Risposta: {0}. Premi ENTER per continuare
                    Arca.WaitingKey("ENTER");
                    return status;
            }
        }

        string DrumPhotoVerify(ref Arca.SPhoto photo,string drum, int value)
        {
            string range = "";
            string result="OK";
            photo.adjMax = Convert.ToInt16(iniConfig.GetValue("drumPhotoRange", photo.name + "max"));
            photo.adjMin = Convert.ToInt16(iniConfig.GetValue("drumPhotoRange", photo.name + "min"));
            range = photo.adjMin + " - " + photo.adjMax;
            photo.value = value;
            if ((photo.value >= photo.adjMin) && (photo.value <= photo.adjMax))
            {
                photo.adjResult = "OK";
            }
            else
            {
                result = "KO";
                photo.adjResult = "KO";
            }
            dgValues.Rows.Add("Cassette " + drum + " - " + photo.name, photo.value, range, photo.adjResult);
            if (photo.adjResult == "KO")
            {
                //dgValues.Rows[dgValues.RowCount - 1].Cells[1].Style.BackColor = Color.Red;
                dgValues.Rows[dgValues.RowCount - 1].DefaultCellStyle.BackColor = Color.Red;
                dgValues.CurrentCell = dgValues.Rows[dgValues.RowCount - 1].Cells[0];
                dgValues.Rows[dgValues.RowCount - 1].Selected=true;

            }
            return result;
        }

        string TowerPhotoVerify(ref Arca.SPhoto photo, string tower, int value)
        {
            string range = "";
            string result = "OK";
            photo.adjMax = Convert.ToInt16(iniConfig.GetValue("towerPhotoRange", photo.name + "max"));
            photo.adjMin = Convert.ToInt16(iniConfig.GetValue("towerPhotoRange", photo.name + "min"));
            range = photo.adjMin + " - " + photo.adjMax;
            photo.value = value;
            if ((photo.value >= photo.adjMin) && (photo.value <= photo.adjMax))
            {
                photo.adjResult = "OK";
            }
            else
            {
                result = "KO";
                photo.adjResult = "KO";
            }
            dgValues.Rows.Add("Tower " + tower + " - " + photo.name,
                                photo.value, range, photo.adjResult);
            if (photo.adjResult == "KO")
            {
                //dgValues.Rows[dgValues.RowCount - 1].Cells[1].Style.BackColor = Color.Red;
                dgValues.Rows[dgValues.RowCount - 1].DefaultCellStyle.BackColor = Color.Red;
                dgValues.CurrentCell = dgValues.Rows[dgValues.RowCount - 1].Cells[0];
                dgValues.Rows[dgValues.RowCount - 1].Selected = true;

            }
            return result;
        }

        string PhotoCalibration(bool singleTest = false)            
        {
        inizio:
            dgValues.Visible = true;
            dgValues.Rows.Clear();
            Arca.WriteMessage(lblTitle, 3); //PHOTO CALIBRATION
            Arca.WriteMessage(lblMessage, 13); //Photo calibration in corso...
            string result = "OK";
            string command = "E";
            string idMod = "";
            int value = 0;
            int timeOut = 100;
            GetTowerType();
            myRs232.OpenCom();
            int idTower = 3;
            for (int i = 0; i <= 1; i++)
            {
                
                idMod = Arca.Dec2Hex(4+(idTower*2)+i);
                for (int x = 30; x < 34; x++)
                {
                    myRs232.SendCommand(command + idMod + x.ToString(), 4, timeOut);
                    myHcLow.tower[idTower].drum[i].address = idMod;
                    value = Arca.Hex2AsciiDec(myRs232.commandAnswer);
                    
                    switch (x.ToString())
                    {
                        case "30":
                            if (DrumPhotoVerify(ref myHcLow.tower[idTower].drum[i].phIn, idMod, value)=="KO"){result = "KO";}
                            break;
                        case "31":
                            if(DrumPhotoVerify(ref myHcLow.tower[idTower].drum[i].phEmpty, idMod, value) =="KO"){result = "KO";}
                            break;
                        case "32":
                            if(DrumPhotoVerify(ref myHcLow.tower[idTower].drum[i].phFull, idMod, value) =="KO"){result = "KO";}
                            break;
                        case "33":
                            if(DrumPhotoVerify(ref myHcLow.tower[idTower].drum[i].phFill, idMod, value) =="KO"){result = "KO";}
                            break;
                    }
                }
            }
            command = "E1";
            idMod = "03";
            myRs232.SendCommand(command + myHcLow.tower[idTower].phTr1.idPar + idMod, 4, timeOut);
            value = Arca.Hex2AsciiDec(myRs232.commandAnswer);
            
            if (TowerPhotoVerify(ref myHcLow.tower[idTower].phTr1, idMod, value) == "KO")
            {
                result = "KO";

            }
            myRs232.CloseCom();
            dgValues.AutoResizeColumns();
            dgValues.ClearSelection();
            if (result == "KO")
            {
                //fallito - premi un tasto per ripetere, esc per uscire
                Arca.WriteMessage(lblMessage, 503, Arca.warningColor); // "FAILED! Premi un tasto per ripetere, ESC per uscire"
                Arca.WaitingKey();
                dgValues.Rows.Clear();
                goto inizio;
            }
            else
            {
                Arca.WriteMessage(lblMessage, lblMessage.Text += " OK");
                foreach (Control myLabel in panelLeft.Panel2.Controls)
                {
                    if (myLabel.Text == "Photo calibration")
                    {
                        myLabel.ForeColor = Arca.correctColor;
                        break;
                    }
                }
            }
            if (singleTest == false)
            {
                Arca.WaitingTime(1000);
                dgValues.Visible = false;
                //CleanPanel();
            }

            if (singleTest==false)
            {
                string log = "";
                for (int iTower=0; iTower<=3; iTower++)
                {
                    Arca.LogWrite(fileLogName, ",2C" + iTower + myHcLow.tower[iTower].phTr1.value);
                    for (int iCass=0; iCass<=1; iCass++)
                    {
                        log = ",2" + Convert.ToString(iTower * 2 + iCass);
                        Arca.LogWrite(fileLogName, log + "0" + myHcLow.tower[iTower].drum[iCass].phIn.value);
                        Arca.LogWrite(fileLogName, log + "1" + myHcLow.tower[iTower].drum[iCass].phEmpty.value);
                        Arca.LogWrite(fileLogName, log + "2" + myHcLow.tower[iTower].drum[iCass].phFull.value);
                        Arca.LogWrite(fileLogName, log + "3" + myHcLow.tower[iTower].drum[iCass].phFill.value);
                    }
                }
            }

            return result;
        }

        private void TestResult(string labelText, string result, bool singleTest)
        {
            if (result == "KO")
            {
                Arca.WriteMessage(lblMessage, 504, Arca.warningColor); //  Testo FALLITO!
                Arca.WaitingKey();
                dgValues.Rows.Clear();
            }
            else
            {
                Arca.WriteMessage(lblMessage, lblMessage.Text += " OK");
                foreach (Control myLabel in panelLeft.Panel2.Controls)
                {
                    if (myLabel.Text == labelText)
                    {
                        myLabel.ForeColor = Arca.correctColor;
                        break;
                    }
                }
            }
            if (singleTest == false)
            {
                Arca.WaitingTime(1000);
                dgValues.Visible = false;
                CleanPanel();
            }
        }

        string Preset(bool singleTest = false)
        {
            // D106 1byte   1sec    Preset

            Arca.WriteMessage(lblTitle, 4); //PRESET
            Arca.WriteMessage(lblMessage, 14); //Preset in corso...
            string command = "D106";
            int timeOut = 1000;
            myRs232.OpenCom();
            myRs232.SendCommand(command, 1, timeOut);
            while (myRs232.commandAnswer == "41")
                {
                    Arca.WaitingTime(250);
                    myRs232.SendCommand("F1");
                }
            myRs232.CloseCom();
            status = myRs232.commandAnswer;
            if (status == "KO" || status=="NO ANSWER" )
            {
                Arca.WriteMessage(lblMessage, 504, Arca.warningColor); //  Testo FALLITO!
                Arca.WaitingKey();
                //dgValues.Rows.Clear();
            }
            else
            {
                Arca.WriteMessage(lblMessage, lblMessage.Text += " OK");
                foreach (Control myLabel in panelLeft.Panel2.Controls)
                {
                    if (myLabel.Text == "Preset")
                    {
                        myLabel.ForeColor = Arca.correctColor;
                        break;
                    }
                }
            }
            if (singleTest == false)
            {
                Arca.LogWrite(fileLogName, ",405" + status);
                Arca.WaitingTime(1000);
                dgValues.Visible = false;
                //CleanPanel();
            }

            
            switch (status)
            {
                case "40":
                    lblMessage.Text += " " + status + " - " + iniStatus.GetValue("SafeStatus", status);
                    return "OK";
                default:
                    Arca.WriteMessage(lblMessage, 501,Arca.messageColor, status + " - " + iniStatus.GetValue("SafeStatus", status)); //FALLITO! Risposta: {0}. Premi ENTER per continuare
                    Arca.WaitingKey("ENTER");
                    return myRs232.commandAnswer;
            }
            
        }

        string SafeStatus(bool singleTest = false)
        {
            //F1   1byte   0.1sec  Get safe status
            Arca.WriteMessage(lblMessage, 15); //Richiesta stato safe in corso...
            Arca.WriteMessage(lblTitle, 5); //SAFE STATUS

            myRs232.OpenCom();
            string command = "F1";
            int timeOut = 100;
            myRs232.SendCommand(command, 1, timeOut);
            myRs232.CloseCom();
            status = myRs232.commandAnswer;
            if (singleTest==false)
            {
                Arca.LogWrite(fileLogName, ",406" + status);
            }
            switch (status)
            {
                case "40":
                    lblMessage.Text += " " + status + " - " + iniStatus.GetValue("SafeStatus", status);
                    return "OK";
                default:
                    Arca.WriteMessage(lblMessage, 501, Arca.messageColor, status + " - " + iniStatus.GetValue("SafeStatus", status)); //FALLITO! Risposta: {0}. Premi ENTER per continuare
                    Arca.WaitingKey("ENTER");
                    return myRs232.commandAnswer;
            }

        }

        string CassetteStatus(string name, bool singleTest = false)
        {
            //Fn   1byte   0.1sec  Get cassette status
            Arca.WriteMessage(lblTitle, 6); //CASSETTES STATUS
            Arca.WriteMessage(lblMessage, 16,Arca.messageColor,name); //Richiesta stato cassetto {0} in corso...
            string address = "";
            switch (name.ToUpper())
            {
                case "A":
                    address = "4";
                    break;
                case "B":
                    address = "5";
                    break;
                case "C":
                    address = "6";
                    break;
                case "D":
                    address = "7";
                    break;
                case "E":
                    address = "8";
                    break;
                case "F":
                    address = "9";
                    break;
                case "G":
                    address = "A";
                    break;
                case "H":
                    address = "B";
                    break;
            }
            
            string command = "F";
            int timeOut = 100;
            myRs232.OpenCom();
            myRs232.SendCommand(command + address, 1, timeOut);
            myRs232.CloseCom();
            status = myRs232.commandAnswer;
            Arca.LogWrite(fileLogName, ",41" + address + status);
            Arca.WaitingTime(200);
            switch (status)
            {
                case "40":
                case "05":
                    Arca.WriteMessage(lblMessage, lblMessage.Text + " " + status + " - " + iniStatus.GetValue("CassetteStatus", status));
                    return "OK";
                default:
                    Arca.WriteMessage(lblMessage, 501, Arca.messageColor, status + " - " + iniStatus.GetValue("SafeStatus", status)); //FALLITO! Risposta: {0}. Premi ENTER per continuare
                    Arca.WaitingKey("ENTER");
                    return myRs232.commandAnswer;
            }
            
        }

        string GetTowerType()
        {
            myRs232.OpenCom();
            myRs232.SendCommand("E138", 1, 100);
            status = myRs232.commandAnswer;
            myRs232.CloseCom();
            switch (status)
            {
                case "30":
                    myHcLow.towerInTest = "MIDDLE";
                    return "MIDDLE";
                case "31":
                    myHcLow.towerInTest = "NOTMIDDLE";
                    return "NOTMIDDLE";
                default:
                    return "KO";
            }
        }
        string HorizzonatalTransportMotor(bool singleTest = false)
        {
            /* E138     1byte   0.1sec  Get tower type
             * D16502   1byte   3sec    Hor motor move
             */
            string key = "";
            Arca.WriteMessage(lblMessage, 17); //Avvio motore trasporto orizzontale in corso...
            Arca.WriteMessage(lblTitle, 7); //HORIZZONTAL TRANSPORT MOTOR
            status = GetTowerType();

            if (status == "NOTMIDDLE")
            {
                myRs232.OpenCom();
                start:
                myRs232.SendCommand("D16502", 1, 3000);
                status = myRs232.commandAnswer;
                myRs232.CloseCom();
                if (status=="40")
                {

                    lblMessage.Text += " " + status + " - " + iniStatus.GetValue("SafeStatus", status);
                    Arca.WriteMessage(lblMessage, 59); //Premere un tasto se il motore ha funzionato correttamente, 'R' per ripetere, ESC per uscire
                    key = Arca.WaitingKey();
                    if (key=="ESC")
                    {
                        return "KO";
                    }
                    if (key == "R") { goto start; }
                    //il motore ha funzionato correttamente?? continua/riprova/esci
                }
                else
                {
                    Arca.WriteMessage(lblMessage, 501, Arca.messageColor, status + " - " + iniStatus.GetValue("SafeStatus", status)); //FALLITO! Risposta: {0}. Premi ENTER per continuare
                    Arca.WaitingKey("ENTER");
                }
            }
            else
            {
                Arca.WriteMessage(lblMessage, 29); // La torre collegata è una "MIDDLE", non è possibile eseguire il test
                Arca.WaitingTime(1000);
            }

            if (singleTest==false)
            {
                Arca.LogWrite(fileLogName, ",303" + status);
            }
            
            return "OK";
            
        }

        string MiddleTowerSorter(bool singleTest = false)
        {
            /* E138     1byte   0.1sec  Get tower type
             * D17C0300 1byte   1       Open sorter
             * E12F     7byte   0.1     Get tower status
             * D17C0301 1byte   1       Close sorter
             * E12F     7byte   0.1     Get tower status
             * 
             */

            Arca.WriteMessage(lblTitle, 8); //MIDDLE TOWER SORTER
            Arca.WriteMessage(lblMessage, 18); //Test sorter torre centrale in corso...
            status = GetTowerType();

            if (status=="MIDDLE")
            {
                int srtOpenMin = Convert.ToInt16(iniConfig.GetValue("towerTimeRange", "srtOpenMin"));
                int srtOpenMax = Convert.ToInt16(iniConfig.GetValue("towerTimeRange", "srtOpenMax"));
                int srtCloseMin = Convert.ToInt16(iniConfig.GetValue("towerTimeRange", "srtCloseMin"));
                int srtCloseMax = Convert.ToInt16(iniConfig.GetValue("towerTimeRange", "srtCloseMax"));
                sorterOpen:
                myRs232.SendCommand("D17C0300", 1, 1000);
                status = myRs232.commandAnswer;
                if (status != "40")
                {
                    Arca.WriteMessage(lblMessage, 513, Arca.errorColor, status, iniStatus.GetValue("SafeStatus", status));
                    switch(Arca.WaitingKey())
                    {
                        case "P":
                            Preset();
                            goto sorterOpen;
                        case "ESCE":
                            goto end;
                        default:
                            goto sorterOpen;
                    }
                }
                getError:
                GetModuleError();
                status = myHcLow.tower[3].status;
                switch (status)
                {
                    case "02":
                        Arca.WaitingTime(200);
                        goto getError;
                    case "04":
                        closeSorter:
                        myRs232.SendCommand("D17C0301", 1, 1000); 
                        status = myRs232.commandAnswer;
                        if (status=="40")
                        {
                            goto getTime;
                        }
                        Arca.WriteMessage(lblMessage, 513, Arca.errorColor, status, iniStatus.GetValue("SafeStatus", status)); //FALLITO! Safe in stato {0} - {1}. Premere un tasto per ripetere, P per eseguire un PRESET e riprovare, ESC per uscire.
                        switch (Arca.WaitingKey())
                        {
                            case "P":
                                Preset();
                                goto closeSorter;
                            case "ESCE":
                                goto end;
                            default:
                                goto getError;
                        }
                    default:
                        Arca.WriteMessage(lblMessage, 520, Arca.errorColor, status, iniStatus.GetValue("TowerStatus", status)); //FALLITO! Tower in stato {0} - {1}. Premere un tasto per ripetere, P per eseguire un PRESET e riprovare, ESC per uscire.
                        switch (Arca.WaitingKey())
                        {
                            case "P":
                                Preset();
                                goto getError;
                            case "ESCE":
                                goto end;
                            default:
                                goto getError;
                        }
                }
            getTime:
                Arca.WaitingTime(1000);
                myRs232.SendCommand("E15903", 8, 100);
                status = myRs232.commandAnswer;
                myHcLow.tower[3].sorter.openingTime = Convert.ToInt16(Arca.Hex2Ascii(status.Substring(0, 8)));
                myHcLow.tower[3].sorter.closingTime = Convert.ToInt16(Arca.Hex2Ascii(status.Substring(8, 8)));
                if (myHcLow.tower[3].sorter.openingTime < srtOpenMin || myHcLow.tower[3].sorter.openingTime > srtOpenMax)
                {
                    status = "KO";
                }

                if (myHcLow.tower[3].sorter.closingTime < srtCloseMin || myHcLow.tower[3].sorter.closingTime > srtCloseMax)
                {
                    status = "KO";
                }
                if (status != "KO") { status = "40"; }


            end:
                myRs232.CloseCom();
                switch (status)
                {
                    case "40":
                        lblMessage.Text += " " + status;
                        return "OK";
                    default:
                        Arca.WriteMessage(lblMessage, 501, Arca.messageColor, status); //FALLITO! Risposta: {0}. Premi ENTER per continuare
                        Arca.WaitingKey("ENTER");
                        return myRs232.commandAnswer;
                }
            }
            else
            {
                Arca.WriteMessage(lblMessage, 30); //La torre collegata NON è una "MIDDLE", non è possibile eseguire il test
                Arca.WaitingTime(1000);
                return "OK";
            }
        }

        string CassetteSorter(bool singleTest = false)
        {
            /* DA01     0       1       Open sorter
            * ????     ?       ?       Drum status
            * DA02     0       1       Close sorter
            */
            Arca.WriteMessage(lblTitle, 9); //CASSETTES SORTER
            Arca.WriteMessage(lblMessage, 19); //Test sorter cassetti in corso...
            int srtOpenMin = Convert.ToInt16(iniConfig.GetValue("drumTimeRange", "srtOpenMin"));
            int srtOpenMax = Convert.ToInt16(iniConfig.GetValue("drumTimeRange", "srtOpenMax"));
            int srtCloseMin = Convert.ToInt16(iniConfig.GetValue("drumTimeRange", "srtCloseMin"));
            int srtCloseMax = Convert.ToInt16(iniConfig.GetValue("drumTimeRange", "srtCloseMax"));
            string srtTiming;
            myRs232.OpenCom();
            myRs232.SendCommand("DA01",0, 1000);
            myRs232.SendCommand("FA",1, 100);
            status = myRs232.commandAnswer;
            switch (status)
            {
                case "40":
                case "01":
                case "05":
                    myRs232.SendCommand("DA02", 0, 1000);
                    myRs232.SendCommand("FA", 1, 100);
                    status = myRs232.commandAnswer;
                    break;
                default:
                    goto end;
            }

            switch (status)
            {
                case "40":
                case "01":
                case "05":
                    srtTiming = myRs232.SendCommand("EA39", 8, 100);
                    myHcLow.tower[3].drum[1].sorter.openingTime = Convert.ToInt16(Arca.Hex2Ascii(srtTiming.Substring(0, 8)));
                    myHcLow.tower[3].drum[1].sorter.closingTime = Convert.ToInt16(Arca.Hex2Ascii(srtTiming.Substring(8, 8)));
                    Arca.WriteMessage(lblMessage, 31, Arca.messageColor, myHcLow.tower[3].drum[1].sorter.openingTime, myHcLow.tower[3].drum[1].sorter.closingTime);
                    break;
                default:
                    goto end;
            }
            
            if (myHcLow.tower[3].drum[1].sorter.openingTime < srtOpenMin || myHcLow.tower[3].drum[1].sorter.openingTime > srtOpenMax)
            {
                status = "KO";
            }
            if (myHcLow.tower[3].drum[1].sorter.closingTime < srtCloseMin || myHcLow.tower[3].drum[1].sorter.closingTime > srtCloseMax)
            {
                status = "KO";
            }
            if (status != "KO")
            {
                status = "OK";
            }


        end:
            myRs232.CloseCom();
            lblMessage.Text += status;
            return status;
        }

        string CassetteTapeLenght(string drum = "ALL", bool singleTest = false)
        {

            /* 9nPswXxVal   0   ?       Set motor current to 80% (9n3132333435362330303830)
             * DA20         0   ?       Unwind bundle
             * E13B         6   ?       Get bundle lenght (after unwind)
             * DA04         0   ?       Wind bundle
             * E13C         6   ?       Get bundle lenght (after wind)
             * 9nPswXxVal   0   ?       Set motor current to 100% (9n3132333435362330313030)
             */

            Arca.WriteMessage(lblTitle, 10); //CASSETTES	TAPE LENGHT
            Arca.WriteMessage(lblMessage, 20); //Test lunghezza nastro cassetti in corso...
            int windTapeMin = Convert.ToInt32(iniConfig.GetValue("drumTapeLenght","tapeLenghtMin"));
            int windTapeMax = Convert.ToInt32(iniConfig.GetValue("drumTapeLenght", "tapeLenghtMax"));
            int tapeMaxDiff = Convert.ToInt16(iniConfig.GetValue("drumTapeLenght", "maxDiff"));
            int lenght = 0;
            string stateA = "40";
            string stateB = "40";
            string test = "OK";
            string motorCurrent = "303830";
            dgValues.Rows.Clear();
            dgValues.ColumnCount = 7;
            dgValues.Visible = true;
            dgValues.Rows.Add("Cass.", "Wind Lenght", "Unwind Lenght", "Range", "Difference", "Max", "State");
            dgValues.Rows.Add("0", "-", "-", windTapeMin + "-" + windTapeMax, "-", tapeMaxDiff);
            dgValues.Rows.Add("1", "-", "-", windTapeMin + "-" + windTapeMax, "-", tapeMaxDiff);
            dgValues.AutoResizeColumns();
            myRs232.OpenCom();
            Arca.WriteMessage(lblMessage, 32, Arca.messageColor, Arca.Hex2Ascii(motorCurrent)); //Impostazione corrente motori a {0}%.
            myRs232.SendCommand("9A3132333435362330" + motorCurrent, 0, 100);
            wind: 
            Arca.WriteMessage(lblMessage, 33,Arca.messageColor,""); //Avvolgimento bandella cassetti in corso... {0}
            dgValues.Rows[1].Cells[1].Style.BackColor = Color.PaleGoldenrod;
            dgValues.Rows[2].Cells[1].Style.BackColor = Color.PaleGoldenrod;
            switch (drum)
            {
                case "G":
                    myRs232.SendCommand("DA04", 0, 100);
                    dgValues.Rows[1].Cells[1].Value = "In progress...";
                    break;
                case "H":
                    myRs232.SendCommand("DB04", 0, 100);
                    dgValues.Rows[2].Cells[1].Value = "In progress...";
                    break;
                default:
                    myRs232.SendCommand("DA04", 0, 100);
                    myRs232.SendCommand("DB04", 0, 100);
                    dgValues.Rows[1].Cells[1].Value = "In progress...";
                    dgValues.Rows[2].Cells[1].Value = "In progress...";
                    break;
            }
            dgValues.Refresh();
            lenght = 0;
        stateOnWind:
            if (drum != "H")
            {
                myRs232.SendCommand("FA", 1, 100);
                stateA = myRs232.commandAnswer;
                dgValues.Rows[1].Cells[6].Value = stateA;
            }
            if (drum != "G")
            {
                myRs232.SendCommand("FB", 1, 100);
                stateB = myRs232.commandAnswer;
                dgValues.Rows[2].Cells[6].Value = stateB;
            }
            Arca.WaitingTime(500);

            if (stateA == "41" || stateB == "41")
            {
                lenght += 249;
                Arca.WriteMessage(lblMessage, 33, Arca.messageColor, lenght.ToString() + "mm"); //Avvolgimento bandella cassetti in corso... {0}
                goto stateOnWind;
            }
            
            if ((stateA != "40" & stateA != "05" & stateA != "01") || (stateB != "40" & stateB != "05" & stateB != "01"))
            {
                Arca.WriteMessage(lblMessage, 505, Arca.errorColor, stateA, stateB); //FALLITO! Cassetto G in stato {0} e Cassetto H in stato {1}. Premere un tasto per ripetere, ESC per uscire
                if (Arca.WaitingKey()!="ESC")
                {
                    goto wind;
                }
                test = "KO";
                goto end;
            }
            Arca.WriteMessage(lblMessage, 35); //Lettura misurazione bandelle appena avvolte
            if (drum != "H")
            {
                myRs232.SendCommand("EA3B", 6, 100);
                myHcLow.tower[3].drum[0].windTapeLenght = Convert.ToInt32(Arca.Hex2Ascii(myRs232.commandAnswer));
                dgValues.Rows[1].Cells[1].Value = myHcLow.tower[3].drum[0].windTapeLenght;
            }
            if (drum != "G")
            {
                myRs232.SendCommand("EB3B", 6, 100);
                myHcLow.tower[3].drum[1].windTapeLenght = Convert.ToInt32(Arca.Hex2Ascii(myRs232.commandAnswer));
                dgValues.Rows[2].Cells[1].Value = myHcLow.tower[3].drum[1].windTapeLenght;
            }
            dgValues.Rows[1].Cells[1].Style.BackColor = Color.White;
            dgValues.Rows[2].Cells[1].Style.BackColor = Color.White;
            dgValues.Refresh();

            if (drum != "H")
            {
                if (myHcLow.tower[3].drum[0].windTapeLenght < windTapeMin || myHcLow.tower[3].drum[0].windTapeLenght > windTapeMax)
                {
                    //misura tape wind cass 0 sbagliata 
                    test = "KO";
                    Arca.WriteMessage(lblMessage, 506, Arca.errorColor, "G", myHcLow.tower[3].drum[0].windTapeLenght); //TEST Fallito! Lunghezza bandelle cassetto {0} errata. Lunghezza = {1}mm. Premere un tasto per continuare
                    dgValues.Rows[1].Cells[1].Style.BackColor = Arca.errorColor;
                    Arca.WaitingKey();
                }
            }
            if (drum != "G")
            {
                if (myHcLow.tower[3].drum[1].windTapeLenght < windTapeMin || myHcLow.tower[3].drum[1].windTapeLenght > windTapeMax)
                {
                    //misura tape wind cass 1 sbagliata
                    test = "KO";
                    Arca.WriteMessage(lblMessage, 506, Arca.errorColor, "H", myHcLow.tower[3].drum[1].windTapeLenght); //TEST Fallito! Lunghezza bandelle cassetto {0} errata. Lunghezza = {1}mm. Premere un tasto per continuare
                    dgValues.Rows[2].Cells[1].Style.BackColor = Arca.errorColor;
                    Arca.WaitingKey();
                }
            }
            unwind:
            Arca.WriteMessage(lblMessage, 34); //Svolgimento bandella cassetti in corso...
            dgValues.Rows[1].Cells[2].Style.BackColor = Color.PaleGoldenrod;
            dgValues.Rows[2].Cells[2].Style.BackColor = Color.PaleGoldenrod;
            switch  (drum)
            {
                case "G":
                    myRs232.SendCommand("DA20", 0, 100);
                    dgValues.Rows[1].Cells[2].Value = "In progress...";
                    break;
                case "H":
                    myRs232.SendCommand("DB20", 0, 100);
                    dgValues.Rows[2].Cells[2].Value = "In progress...";
                    break;
                default:
                    myRs232.SendCommand("DA20", 0, 100);
                    myRs232.SendCommand("DB20", 0, 100);
                    dgValues.Rows[1].Cells[2].Value = "In progress...";
                    dgValues.Rows[2].Cells[2].Value = "In progress...";
                    break;
            }
            
            dgValues.Refresh();
            lenght = 0;
        stateOnUnwind:
            stateA = "40";
            stateB = "40";
            if (drum != "H")
            {
                myRs232.SendCommand("FA", 1, 100);
                stateA = myRs232.commandAnswer;
                dgValues.Rows[1].Cells[6].Value = stateA;
            }
            if (drum != "G")
            {
                myRs232.SendCommand("FB", 1, 100);
                stateB = myRs232.commandAnswer;
                dgValues.Rows[2].Cells[6].Value = stateB;
            }
            Arca.WaitingTime(500);
            if (stateA == "41" || stateB == "41")
            {
                lenght += 249;
                Arca.WriteMessage(lblMessage, 33, Arca.messageColor, lenght.ToString() + "mm"); //Avvolgimento bandella cassetti in corso... {0}
                goto stateOnUnwind;
            }
            if ((stateA != "40" & stateA != "05" & stateA != "01")  || (stateB != "40" & stateB != "05" & stateB != "01"))
            {
                Arca.WriteMessage(lblMessage, 505, Arca.errorColor, stateA, stateB); //FALLITO! Cassetto G in stato {0} e Cassetto H in stato {1}. Premere un tasto per ripetere, ESC per uscire
                if (Arca.WaitingKey() != "ESC")
                {
                    goto unwind;
                }
                test = "KO";
                goto end;
            }
            Arca.WriteMessage(lblMessage, 36); //Lettura misurazione bandelle appena svolte
            dgValues.Rows[1].Cells[2].Style.BackColor = Color.White;
            dgValues.Rows[2].Cells[2].Style.BackColor = Color.White;

            if (drum != "H")
            {
                myRs232.SendCommand("EA3C", 6, 100);
                myHcLow.tower[3].drum[0].unwindTapeLenght = Convert.ToInt32(Arca.Hex2Ascii(myRs232.commandAnswer));
                dgValues.Rows[1].Cells[2].Value = myHcLow.tower[3].drum[0].unwindTapeLenght;
            }
            if (drum != "G")
            {
                myRs232.SendCommand("EB3C", 6, 100);
                myHcLow.tower[3].drum[1].unwindTapeLenght = Convert.ToInt32(Arca.Hex2Ascii(myRs232.commandAnswer));
                dgValues.Rows[2].Cells[2].Value = myHcLow.tower[3].drum[1].unwindTapeLenght;
            }
            dgValues.Refresh();

            if (drum != "H")
            {
                if (myHcLow.tower[3].drum[0].unwindTapeLenght < windTapeMin || myHcLow.tower[3].drum[0].unwindTapeLenght > windTapeMax)
                {
                    //misura tape unwind cass 0 sbagliata
                    test = "KO";
                    Arca.WriteMessage(lblMessage, 507, Arca.errorColor, "G", myHcLow.tower[3].drum[0].windTapeLenght); //TEST Fallito! Lunghezza bandelle in svolgimento cassetto {0} errata. Lunghezza = {1}mm. Premere un tasto per continuare
                    dgValues.Rows[1].Cells[2].Style.BackColor = Arca.errorColor;
                    dgValues.Refresh();
                    Arca.WaitingKey();
                }
            }

            if (drum != "G")
            {
                if (myHcLow.tower[3].drum[1].unwindTapeLenght < windTapeMin || myHcLow.tower[3].drum[1].unwindTapeLenght > windTapeMax)
                {
                    //misura tape unwind cass 1 sbagliata
                    test = "KO";
                    Arca.WriteMessage(lblMessage, 507, Arca.errorColor, "H", myHcLow.tower[3].drum[1].windTapeLenght); //TEST Fallito! Lunghezza bandelle in svolgimento cassetto {0} errata. Lunghezza = {1}mm. Premere un tasto per continuare
                    dgValues.Rows[2].Cells[2].Style.BackColor = Arca.errorColor;
                    dgValues.Refresh();
                    Arca.WaitingKey();
                }
            }

            myHcLow.tower[3].drum[0].tapeLenghtDiff = Math.Abs(myHcLow.tower[3].drum[0].unwindTapeLenght - myHcLow.tower[3].drum[0].windTapeLenght);
            myHcLow.tower[3].drum[1].tapeLenghtDiff = Math.Abs(myHcLow.tower[3].drum[1].unwindTapeLenght - myHcLow.tower[3].drum[1].windTapeLenght);
            dgValues.Rows[1].Cells[4].Value = myHcLow.tower[3].drum[0].tapeLenghtDiff;
            dgValues.Rows[2].Cells[4].Value = myHcLow.tower[3].drum[1].tapeLenghtDiff;

            if (drum != "H")
            {
                if (myHcLow.tower[3].drum[0].tapeLenghtDiff > tapeMaxDiff)
                {
                    //diff cass 0 troppo alta
                    Arca.WriteMessage(lblMessage, 508, Arca.errorColor, "G", myHcLow.tower[3].drum[0].tapeLenghtDiff);//Differenza di mirura tra avvolgimento e svolgimento bandella cassetto {0} eccessiva({0}mm). Premere un tasto per ripetere la misurazione, ESC per uscire
                    dgValues.Rows[1].Cells[5].Style.BackColor = Arca.errorColor;
                    if (Arca.WaitingKey() != "ESC")
                    {
                        goto wind;
                    }
                    test = "KO";
                }
            }
            if (drum != "G")
            {
                if (myHcLow.tower[3].drum[1].tapeLenghtDiff > tapeMaxDiff)
                {
                    //diff cass 1 troppo alta
                    Arca.WriteMessage(lblMessage, 508, Arca.errorColor, "H", myHcLow.tower[3].drum[1].tapeLenghtDiff);//Differenza di mirura tra avvolgimento e svolgimento bandella cassetto {0} eccessiva({0}mm). Premere un tasto per ripetere la misurazione, ESC per uscire
                    dgValues.Rows[2].Cells[5].Style.BackColor = Arca.errorColor;
                    if (Arca.WaitingKey() != "ESC")
                    {
                        goto wind;
                    }
                    test = "KO";
                }
            }

            motorCurrent = "313030";
            Arca.WriteMessage(lblMessage, 32, Arca.messageColor, Arca.Hex2Ascii(motorCurrent)); //Impostazione corrente motori a {0}%.
            myRs232.SendCommand("9A3132333435362330" + motorCurrent, 0, 100);

        end:
            dgValues.Refresh();
            myRs232.CloseCom();
            if (singleTest==false)
            {
                Arca.LogWrite(fileLogName, ",426" + myHcLow.tower[3].drum[0].windTapeLenght);
                Arca.LogWrite(fileLogName, ",432" + myHcLow.tower[3].drum[0].unwindTapeLenght);
                Arca.LogWrite(fileLogName, ",427" + myHcLow.tower[3].drum[1].windTapeLenght);
                Arca.LogWrite(fileLogName, ",433" + myHcLow.tower[3].drum[1].unwindTapeLenght);
            }
            return test;
        }

        string VerticalTransport(bool singleTest = false)
        {
            /* 91XXITVAL    1   ?       Set motor current to 80% (9144330303830)
             * D17B0300     1   0.1     Move vertical motor in input
             * E12F         7   0.1     Get tower status
             * D17B0302     1   0.1     Stop vertical motor
             * D17B0301     1   0.1     Move vertical motor in output
             * E12F         7   0.1     Get tower status
             * D17B0302     1   0.1     Stop vertical motor
             * 91XXITVAL    1   ?       Set motor current to 100% (9144330313030)
             */
            Arca.WriteMessage(lblTitle, 11); //VERTICAL TRANSPORT
            Arca.WriteMessage(lblMessage, 21); //Test trasporto verticale in corso...
            string motCur = "";
            string test = "OK";
            myRs232.OpenCom();
        SafeState:
            myRs232.SendCommand("F1", 1, 100);
            //safe status 40
            status = myRs232.commandAnswer;
            Arca.WriteMessage(lblMessage, 45, Arca.messageColor, status, iniStatus.GetValue("SafeStatus", status)); //Stato safe: {0} - {1}.
            if (status != "40")
            {
                Arca.WriteMessage(lblMessage, 513, Arca.errorColor, status, iniStatus.GetValue("SafeStatus", status)); //Safe in stato {0} - {1}. Premere un tasto per ripetere, P per eseguire un PRESET, ESC per uscire.
                switch (Arca.WaitingKey())
                {
                    case "P":
                        Preset(true);
                        goto SafeState;
                    case "E":
                        test = "KO";
                        goto endTest;
                    default:
                        goto SafeState;
                }
            }

        set80:
            motCur = Arca.Ascii2Hex("0080");
            Arca.WriteMessage(lblMessage, 32, Arca.messageColor, Arca.Hex2Ascii(motCur)); //Impostazione corrente motori a {0}%.
            myRs232.SendCommand("912F" + motCur, 1, 100);
            //safe status 40
            status = myRs232.commandAnswer;
            Arca.WriteMessage(lblMessage, 45, Arca.messageColor, status, iniStatus.GetValue("SafeStatus", status)); //Stato safe: {0} - {1}.
            if (status != "40")
            {
                Arca.WriteMessage(lblMessage, 514, Arca.errorColor, Arca.Hex2Ascii(motCur), status, iniStatus.GetValue("SafeStatus", status)); //FALLITO! Impostazione corrente motore al {0} non riuscita. Safe in stato {1} - {2} Premere un tasto per ripetere, ESC per uscire
                switch (Arca.WaitingKey())
                {
                    case "P":
                        Preset(true);
                        goto set80;
                    case "ESC":
                        test = "KO";
                        goto endTest;
                    default:
                        goto set80;
                }
            }

        motorIn:
            Preset();
            Arca.WriteMessage(lblMessage, 37); //Movimento motore verticale in direzione "input"
            myRs232.SendCommand("D17B0300", 1, 100);
            //safe status 40
            status = myRs232.commandAnswer;
            Arca.WriteMessage(lblMessage, 45, Arca.messageColor, status, iniStatus.GetValue("SafeStatus", status)); //Stato safe: {0} - {1}.
            if (status != "40")
            {
                Arca.WriteMessage(lblMessage, 515, Arca.errorColor, status, iniStatus.GetValue("SafeStatus", status)); //FALLITO! Test motore in direzione IN non riuscito. Safe in stato {1} - {2} Premere un tasto per ripetere, ESC per uscire
                switch (Arca.WaitingKey())
                {
                    //case "P":
                    //Preset(true);
                    //goto motorIn;
                    case "ESC":
                        test = "KO";
                        goto endTest;
                    default:
                        goto motorIn;
                }
            }

        statusIn:
            GetModuleError();
            myRs232.OpenCom();
            int timeToWait = Convert.ToInt16(iniConfig.GetValue("towerTimeRange", "verTrMotIn"));
            double timeElapsed = 0;
            if (myHcLow.tower[3].status == "02")
            {
                while (timeElapsed < timeToWait)
                {
                    Arca.WaitingTime(100);
                    timeElapsed += 100;
                    Arca.WriteMessage(lblMessage, 44, Arca.messageColor, timeElapsed / 1000, timeToWait/1000); //Il motore gira da {0} secondi (durata test {1} secondi)
                }
                timeElapsed = 0;
            }
            else
            {
                Arca.WriteMessage(lblMessage, 517, Arca.errorColor, myHcLow.tower[3].status, iniStatus.GetValue("TowerStatus", myHcLow.tower[3].status));
                if (Arca.WaitingKey() != "ESC")
                {
                    goto statusIn;
                }
                test = "KO";
            }

        motorStopIn:
            Arca.WriteMessage(lblMessage, 39); //Arresto motore verticale
            myRs232.SendCommand("D17B0302", 1, 100);
            
            //safe status 40
             status = myRs232.commandAnswer;
            Arca.WriteMessage(lblMessage, 45, Arca.messageColor, status, iniStatus.GetValue("SafeStatus", status)); //Stato safe: {0} - {1}.
            Arca.WaitingTime(1000);
            if (status != "40")
            {
                Arca.WriteMessage(lblMessage, 516, Arca.errorColor, status, iniStatus.GetValue("SafeStatus", status)); //FALLITO! Test motore in direzione IN non riuscito. Safe in stato {1} - {2} Premere un tasto per ripetere, ESC per uscire
                switch (Arca.WaitingKey())
                {
                    //case "P":
                    //    Preset(true);
                    //    goto motorStopIn;
                    case "ESC":
                        test = "KO";
                        goto endTest;
                    default:
                        goto motorStopIn;
                }
            }

        motorOut:
            Arca.WriteMessage(lblMessage, 38); //Movimento motore verticale in direzione "output"
            myRs232.SendCommand("D17B0301", 1, 100);
            //safe status 40
            status = myRs232.commandAnswer;
            Arca.WriteMessage(lblMessage, 45, Arca.messageColor, status, iniStatus.GetValue("SafeStatus", status)); //Stato safe: {0} - {1}.
            if (status != "40")
            {
                Arca.WriteMessage(lblMessage, 518, Arca.errorColor, status, iniStatus.GetValue("SafeStatus", status)); //FALLITO! Test motore in direzione IN non riuscito. Safe in stato {1} - {2} Premere un tasto per ripetere, ESC per uscire
                switch (Arca.WaitingKey())
                {
                    //case "P":
                    //    Preset(true);
                    //    goto motorStopIn;
                    case "ESC":
                        test = "KO";
                        goto endTest;
                    default:
                        goto motorOut;
                }
            }

        statusOut:
            GetModuleError();
            myRs232.OpenCom();
            timeToWait = Convert.ToInt16(iniConfig.GetValue("towerTimeRange", "verTrMotIn"));
            timeElapsed = 0;
            if (myHcLow.tower[3].status == "02")
            {
                while (timeElapsed < timeToWait)
                {
                    Arca.WaitingTime(100);
                    timeElapsed += 100;
                    Arca.WriteMessage(lblMessage, 44, Arca.messageColor, timeElapsed / 1000, timeToWait / 1000); //Il motore gira da {0} secondi (durata test {1} secondi)
                }
                timeElapsed = 0;
            }
            else
            {
                Arca.WriteMessage(lblMessage, 517, Arca.errorColor, myHcLow.tower[3].status, iniStatus.GetValue("TowerStatus", myHcLow.tower[3].status)); //FALLITO! Torre in stato {0}. Premere un tasto per ripetere, ESC per uscire
                if (Arca.WaitingKey() != "ESC")
                {
                    goto statusOut;
                }
                test = "KO";
            }

        motorStopOut:
            Arca.WriteMessage(lblMessage, 39); //Arresto motore verticale
            myRs232.SendCommand("D17B0302", 1, 100);
            //tower status 40
            status = myRs232.commandAnswer;
            Arca.WriteMessage(lblMessage, 45, Arca.messageColor, status, iniStatus.GetValue("SafeStatus", status)); //Stato safe: {0} - {1}.
            if (status != "40")
            {
                Arca.WriteMessage(lblMessage, 516, Arca.errorColor, status, iniStatus.GetValue("SafeStatus", status)); //FALLITO! Test motore in direzione IN non riuscito. Safe in stato {1} - {2} Premere un tasto per ripetere, ESC per uscire
                switch (Arca.WaitingKey())
                {
                    //case "P":
                    //    Preset(true);
                    //    goto motorStopIn;
                    case "ESC":
                        test = "KO";
                        goto endTest;
                    default:
                        goto motorStopOut;
                }
            }
        set100:
            motCur = Arca.Ascii2Hex("0100");
            Arca.WriteMessage(lblMessage, 32, Arca.messageColor, Arca.Hex2Ascii(motCur)); //Impostazione corrente motori a {0}%.
            myRs232.SendCommand("912F" + motCur, 1, 5000);
            status = myRs232.commandAnswer;
            Arca.WriteMessage(lblMessage, 45, Arca.messageColor, status, iniStatus.GetValue("SafeStatus", status)); //Stato safe: {0} - {1}.
            if (status != "40")
            {
                Arca.WriteMessage(lblMessage, 514, Arca.errorColor, Arca.Hex2Ascii(motCur), status, iniStatus.GetValue("SafeStatus", status)); //FALLITO! Impostazione corrente motore al {0} non riuscita. Safe in stato {1} - {2} Premere un tasto per ripetere, ESC per uscire
                switch (Arca.WaitingKey())
                {
                    case "P":
                        Preset(true);
                        goto set100;
                    case "ESC":
                        test = "KO";
                        goto endTest;
                    default:
                        goto set100;
                }
            }
        endTest:
            myRs232.CloseCom();
            Arca.WriteMessage(lblMessage, 40); //Test motore verticale eseguito correttamente
            if (singleTest == false)
            {
                Arca.LogWrite(fileLogName, ",304" + test);
            }
            return test;
        }

        public void SingleTestMode(bool activate)
        {
            switch (activate)
            {
                case true:
                    btnStart.Visible = false;
                    grpSerialNumber.Visible = false;
                    Arca.WriteMessage(lblMessage, 28); //Seleziona il singolo test dal menu a tendina
                    break;
                case false:
                    btnStart.Visible = true;
                    grpSerialNumber.Visible = true;
                    Arca.WriteMessage(lblMessage, 27); //Inserire le matricole ed i livelli tecnici, o passa alla modalità singolo test
                    break;
            }
            
        }

        string GetModuleError(bool singleTest = false)
        {
            //E12F         7   0.1     Get tower status
            Arca.WriteMessage(lblTitle, 25); //TOWER STATUS AND ERROR CODE
            Arca.WriteMessage(lblMessage, 22); //Richiesta stato torri e codice errore in corso...
            myRs232.OpenCom();
            myRs232.SendCommand("E12F", 7, 100);
            myRs232.CloseCom();
            string answer = myRs232.commandAnswer;
            if (myRs232.commandAnswer != "NO ANSWER")
            {
                myHcLow.tower[0].status = answer.Substring(0, 2);
                myHcLow.tower[1].status = answer.Substring(2, 2);
                myHcLow.tower[2].status = answer.Substring(4, 2);
                myHcLow.tower[3].status = answer.Substring(6, 2);
                string idMod = answer.Substring(8, 2);
                string Module = iniStatus.GetValue("ModuleIDDec", idMod);
                int errorCode = Arca.Hex2Dec(answer.Substring(10, 2)) * 128 + Arca.Hex2Dec(answer.Substring(12, 2));
                string errorMessage = iniError.GetValue("TowerErrorCode", errorCode.ToString());
                string errorDescription = iniError.GetValue("TowerErrorDescription", errorCode.ToString());
                if (Module == "")
                {
                    Arca.WriteMessage(lblMessage, 26); //'Nessun modulo in errore'
                }
                else
                {
                    Arca.WriteMessage(lblMessage, 502, Arca.errorColor, Module, errorCode.ToString(), errorMessage); //Modulo {0}  errore {1} - {2}
                }
            }
            else
            {
                lblMessage.Text += " - NO ANSWER";
            }
            return answer;
        }

        private void panelMain_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void HcTower_KeyUp(object sender, KeyEventArgs e)
        {
            Arca.keyCodePressed = e.KeyValue;
        }

        private void factoryCalibrationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FactoryCalibration(true);
        }

        private void photoCalibrationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PhotoCalibration(true);
        }

        private void presetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Preset(true);
        }

        private void safeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SafeStatus(true);
        }

        private void drumAToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CassetteStatus("A", true);
        }

        private void drumBToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CassetteStatus("B", true);
        }

        private void drumCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CassetteStatus("C", true);
        }

        private void drumDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CassetteStatus("D", true);
        }

        private void drumEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CassetteStatus("E", true);
        }

        private void drumFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CassetteStatus("F", true);
        }

        private void drumGToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CassetteStatus("G", true);
        }

        private void drumHToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CassetteStatus("H", true);
        }

        private void getModuleErrorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GetModuleError(true);
        }

        private void horizzontalTransportMotorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HorizzonatalTransportMotor(true);
        }

        private void middleTowerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MiddleTowerSorter(true);
        }

        private void cassetteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CassetteSorter(true);
        }

        private void cassetteTapeLenghtToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void txtMain_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtMain_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
                txtMain.Enabled = false;
                switch (txtMain.Text.Length)
                {
                    case 12: //Serial Number
                        //verifica sn
                        Arca.WriteMessage(lblMessage, 41); //Premere INVIO per confermare la matricola od un tasto qualunque per ripetere l'inserimento.
                        if (Arca.WaitingKey() == "ENTER")
                        {
                            //this.Text = "TOWER SERIAL NUMBER: " + txtMain.Text;
                            //myHcLow.towerSN = txtMain.Text;
                            //txtMain.MaxLength = 2;
                            //txtMain.Text = "";
                            //txtMain.Enabled = true;
                            //txtMain.Focus();
                            Arca.WriteMessage(lblMessage, 47); //Inserire il technical level e premere INVIO
                        }
                        else
                        {
                            Arca.WriteMessage(lblMessage, 27); //Inserire la matricola o passa alla modalità singolo test
                            txtMain.Enabled = true;
                            txtMain.Text = "";
                            txtMain.Focus();
                        }
                        break;
                    case 2: //technical level
                        //verifica TL
                        Arca.WriteMessage(lblMessage, 41); //Premere INVIO per confermare la matricola od un tasto qualunque per ripetere l'inserimento.
                        if (Arca.WaitingKey() == "ENTER")
                        {
                            //this.Text += " - TL:" + txtMain.Text;
                            //myHcLow.towerTL = txtMain.Text;
                            //btnStart.Enabled = true;
                            //txtMain.Visible = false;
                            Arca.WriteMessage(lblMessage, 42); //Premere il pulsante per avviare il collaudo guidato
                        }
                        else
                        {
                            Arca.WriteMessage(lblMessage, 47); //Inserire il technical level e premere INVIO
                            txtMain.Enabled = true;
                            txtMain.Text = "";
                            txtMain.Focus();
                        }

                        break;
                    default:
                        Arca.WriteMessage(lblMessage, 509, Arca.errorColor); //La matricola deve essere composta da 12 caratteri. Premere un tasto per ripetere l'inserimento.
                        Arca.WaitingKey();
                        Arca.WriteMessage(lblMessage, 27); //Inserire la matricola o passa alla modalità singolo test
                        txtMain.MaxLength = 12;
                        txtMain.Enabled = true;
                        txtMain.Text = "";
                        txtMain.Focus();
                        break;

                }
            }
        }

        private void verticalTransportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VerticalTransport();
        }


        private void singleTestToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            singleTestToolStripMenuItem1.Checked = !singleTestToolStripMenuItem1.Checked;
            guidedTestToolStripMenuItem.Checked = !singleTestToolStripMenuItem1.Checked;
            if (singleTestToolStripMenuItem1.Checked==true)
            {
                singleTestToolStripMenuItem.Enabled = true;
                SingleTestMode(true);
            }
            else
            {
                singleTestToolStripMenuItem.Enabled = false;
                SingleTestMode(false);
                txtMain.Text = "";
                txtMain.Focus();
            }
        }



        private void singleTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Arca.WriteMessage(lblMessage, 28);
            dgValues.Visible = false;
            grpSingleCommand.Visible = false;
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            
        }

        private void TowerSNCheck(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtTowerSN.Text.Length != 12)
                {
                    //la matricola deve avere 12 caratteri
                    return;
                }
                myHcLow.tower[3].serialNumber = txtTowerSN.Text;
                lblTowerSN.Text = myHcLow.tower[3].serialNumber + "/TL:" + myHcLow.tower[3].mechanicalLevel;
                txtTowerSN.Enabled = false;
                txtTowerTL.Focus();
            }
        }

        private void TowerTLCheck(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtTowerTL.Text.Length != 2 )
                {
                    Arca.WriteMessage(lblMessage, 521, Arca.errorColor); //Il TL deve essere di due caratteri
                    txtTowerTL.Text = "";
                    return;
                }
                myHcLow.tower[3].technicalLevel = txtTowerTL.Text;
                lblTowerSN.Text = myHcLow.tower[3].serialNumber + "/TL:" + myHcLow.tower[3].technicalLevel;
                txtTowerTL.Enabled = false;
                txtCassGSN.Focus();
            }
        }

        private void CassGSNCheck(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtCassGSN.Text.Length != 12)
                {
                    Arca.WriteMessage(lblMessage, 528, Arca.errorColor); //la matricola deve avere 12 caratteri
                    return;
                }
                myHcLow.tower[3].drum[0].serialNumber = txtCassGSN.Text;
                lblCassGSN.Text = myHcLow.tower[3].drum[0].serialNumber + "/TL:" + myHcLow.tower[3].drum[0].mechanicalLevel;
                txtCassGSN.Enabled = false;
                txtCassGTL.Focus();
            }
        }

        private void CassGTLCheck(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtCassGTL.Text.Length != 2 )
                {
                    Arca.WriteMessage(lblMessage, 521, Arca.errorColor); //Il TL deve essere di due caratteri.
                    txtCassGTL.Text = "";
                    return;
                }
                myHcLow.tower[3].drum[0].technicalLevel = txtCassGTL.Text;
                lblCassGSN.Text = myHcLow.tower[3].drum[0].serialNumber + "/TL:" + myHcLow.tower[3].drum[0].technicalLevel;
                txtCassGTL.Enabled = false;
                txtCassHSN.Focus();
            }
        }

        private void CassHSNCheck(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtCassHSN.Text.Length != 12)
                {
                    Arca.WriteMessage(lblMessage, 528, Arca.errorColor); //la matricola deve avere 12 caratteri
                    return;
                }
                myHcLow.tower[3].drum[1].serialNumber = txtCassHSN.Text;
                lblCassHSN.Text = myHcLow.tower[3].drum[1].serialNumber + "/TL:" + myHcLow.tower[3].drum[1].mechanicalLevel;
                txtCassHSN.Enabled = false;
                txtCassHTL.Focus();
            }
        }

        private void CassHTLCheck(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtCassHTL.Text.Length != 2 )
                {
                    Arca.WriteMessage(lblMessage, 521, Arca.errorColor); //Il TL deve essere di due caratteri.
                    txtCassHTL.Text = "";
                    return;
                }
                myHcLow.tower[3].drum[1].technicalLevel = txtCassHTL.Text;
                lblCassHSN.Text = myHcLow.tower[3].drum[1].serialNumber + "/TL:" + myHcLow.tower[3].drum[1].technicalLevel;
                txtCassHTL.Enabled = false;
                txtCassHSN.Focus();
            }
        }

        private void txtTowerSN_EnabledChanged(object sender, EventArgs e)
        {
            
            if (txtTowerSN.Enabled==false & txtTowerTL.Enabled == false & txtCassGSN.Enabled == false & txtCassGTL.Enabled == false & txtCassHSN.Enabled == false & txtCassHTL.Enabled == false)
            {
                Arca.WriteMessage(lblMessage, 55); //Premere INVIO per confermare matricole e livelli tecnici od un qualunque altro tasto per ripetere l'inserimento
                string tasto = Arca.WaitingKey();
                if (tasto == "ENTER")
                {
                    grpSerialNumber.Visible = false;
                    btnStart.Enabled = true;
                    Arca.WriteMessage(lblMessage, 42); //Premere START per avviare il collaudo guidato
                    return;
                }
                txtTowerSN.Text = "";
                txtTowerTL.Text = "";
                txtCassGSN.Text = "";
                txtCassGTL.Text = "";
                txtCassHSN.Text = "";
                txtCassHTL.Text = "";

                
                txtTowerTL.Enabled = true;
                txtCassGSN.Enabled = true;
                txtCassGTL.Enabled = true;
                txtCassHSN.Enabled = true;
                txtCassHTL.Enabled = true;
                txtTowerSN.Enabled = true;
                
                txtTowerSN.Focus();
            }
        }

        private void txtTowerSN_Enter(object sender, EventArgs e)
        {
            Arca.WriteMessage(lblMessage, 51); //Inserire la matricola della torre e premere INVIO
        }

        private void txtTowerTL_Enter(object sender, EventArgs e)
        {
            Arca.WriteMessage(lblMessage, 52); //Inserire il livello tecnico della torre e premere INVIO
        }

        private void txtCassGSN_Enter(object sender, EventArgs e)
        {
            Arca.WriteMessage(lblMessage, 53,Arca.messageColor,"G"); //Inserire la matricola del cassetto {0} e premere INVIO
        }

        private void txtCassHSN_Enter(object sender, EventArgs e)
        {
            Arca.WriteMessage(lblMessage, 53, Arca.messageColor, "H"); //Inserire la matricola del cassetto {0} e premere INVIO
        }

        private void txtCassGTL_Enter(object sender, EventArgs e)
        {
            Arca.WriteMessage(lblMessage, 54, Arca.messageColor, "G"); //Inserire il livello tecnico del cassetto {0} e premere INVIO
        }

        private void txtCassHTL_Enter(object sender, EventArgs e)
        {
            Arca.WriteMessage(lblMessage, 54, Arca.messageColor, "H"); //Inserire il livello tecnico del cassetto {0} e premere INVIO
        }

        private void drumGToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            PinchTest("A",true);
        }

        private void drumHToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            PinchTest("B",true);
        }

        private void TowerTest(object sender, EventArgs e)
        {
            btnStart.Visible = false;
            txtMain.Visible = false;
            string logsLine = "";
            string FailedTest = "";
            string testResult = "PASS";
            logsLine += "004" + myHcLow.tower[3].serialNumber; //Tower Serial Number
            logsLine += ",01F" + myHcLow.tower[3].mechanicalLevel; //Tower Technical level
            logsLine += ",014" + myHcLow.tower[3].drum[0].serialNumber; //Cass G Serial Number 
            logsLine += ",027" + myHcLow.tower[3].drum[0].mechanicalLevel; //Cass G Technical level
            logsLine += ",015" + myHcLow.tower[3].drum[1].serialNumber; //Cass H Serial Number
            logsLine += ",028" + myHcLow.tower[3].drum[1].mechanicalLevel; //Cass H Technical level
            logsLine += ",005" + Application.ProductVersion; //SW Version
            logsLine += ",006" + System.Environment.MachineName; //Computer Name
            logsLine += ",300" + DateTime.Now.Date.ToShortDateString(); //Data Start
            logsLine += ",301" + DateTime.Now.ToLongTimeString(); //Time Start
            //Arca.LogWrite(fileLogName, "", true);
            Arca.LogWrite(fileLogName, logsLine);

        start:
            foreach (Control myLabel in panelLeft.Panel2.Controls)
            {
                if (myLabel.Text != "Test:")
                {
                    myLabel.ForeColor = Arca.errorColor;
                }
            }
            for (int i = 1; i < testNum; i++)
            {
                lblTest[i].ForeColor = Color.LightYellow;
                Arca.WaitingTime(200);

                switch (testTitle[i])
                {
                    case "TLSAVE":
                        if (TLSave() == "OK") { lblTest[i].ForeColor = Color.YellowGreen; }
                        else
                        {
                            lblTest[i].ForeColor = Color.Red;
                            i = testNum;
                            FailedTest += " TL Save /";
                        }
                        break;
                    case "CASSETTEPINCH":
                        if (PinchTest("A") == "OK" & PinchTest("B") == "OK") { lblTest[i].ForeColor = Color.YellowGreen; }
                        else
                        {
                            lblTest[i].ForeColor = Color.Red;
                            i = testNum;
                            FailedTest += " Cassette Pinch /";
                        }
                        break;
                    case "FACTORYCALIBRATION":
                        if (FactoryCalibration() == "OK") { lblTest[i].ForeColor = Color.YellowGreen; }
                        else
                        {
                            lblTest[i].ForeColor = Color.Red;
                            i = testNum;
                            FailedTest += " Factory Calibration /";
                        }
                        break;
                    case "PHOTOCALIBRATION":
                        if (PhotoCalibration() == "OK") { lblTest[i].ForeColor = Color.YellowGreen; }
                        else
                        {
                            lblTest[i].ForeColor = Color.Red;
                            i = testNum;
                            FailedTest += " Photo Calibration /";
                        }
                        break;
                    case "PRESET":
                        if (Preset() == "OK") { lblTest[i].ForeColor = Color.YellowGreen; }
                        else
                        {
                            lblTest[i].ForeColor = Color.Red;
                            i = testNum;
                            FailedTest += " Preset /";
                        }
                        break;
                    case "SAFESTATUS":
                        if (SafeStatus() == "OK") { lblTest[i].ForeColor = Color.YellowGreen; }
                        else
                        {
                            lblTest[i].ForeColor = Color.Red;
                            i = testNum;
                            FailedTest += " Safe Status /";
                        }
                        break;
                    case "CASSETTESTATUS":
                        if (CassetteStatus("G") == "OK" && CassetteStatus("H") == "OK")
                        { lblTest[i].ForeColor = Color.YellowGreen; }
                        else
                        {
                            lblTest[i].ForeColor = Color.Red;
                            i = testNum;
                            FailedTest += " Cassettes status /";
                        }
                        break;
                    case "HORIZZONTALTRANSPORTMOTOR":
                        if (HorizzonatalTransportMotor() == "OK") { lblTest[i].ForeColor = Color.YellowGreen; }
                        else
                        {
                            lblTest[i].ForeColor = Color.Red;
                            i = testNum;
                            FailedTest += " Horizzonatal Tr Motor /";
                        }
                        break;
                    case "MIDDLETOWERSORTER":
                        if (MiddleTowerSorter() == "OK") { lblTest[i].ForeColor = Color.YellowGreen; }
                        else
                        {
                            lblTest[i].ForeColor = Color.Red;
                            i = testNum;
                            FailedTest += " Middle Tw Sorter /";
                        }
                        break;
                    case "CASSETTESORTER":
                        if (CassetteSorter() == "OK") { lblTest[i].ForeColor = Color.YellowGreen; }
                        else
                        {
                            lblTest[i].ForeColor = Color.Red;
                            i = testNum;
                            FailedTest += " Cassette Sorter /";
                        }
                        break;
                    case "CASSETTETAPELENGHT":
                        if (CassetteTapeLenght() == "OK") { lblTest[i].ForeColor = Color.YellowGreen; }
                        else
                        {
                            lblTest[i].ForeColor = Color.Red;
                            i = testNum;
                            FailedTest += " Cassettes Tape Lenght /";
                        }
                        break;
                    case "VERTICALTRANSPORT":
                        if (VerticalTransport() == "OK") { lblTest[i].ForeColor = Color.YellowGreen; }
                        else
                        {
                            lblTest[i].ForeColor = Color.Red;
                            i = testNum;
                            FailedTest += " Vertical Tr Motor /";
                        }
                        break;
                    default:
                        break;
                }
            }
            if (FailedTest.Length == 0)
            {
                Arca.WriteMessage(lblMessage, 43); //Test eseguito correttamente. Premere un tasto per ricominciare
            }
            else
            {
                Arca.WriteMessage(lblMessage, 519, Arca.errorColor, FailedTest); //COLLAUDO FALLITO ({0})!! Premere un tasto per ripetere dall'inizio, ESC per uscire.
                testResult = "KO";
            }

            Arca.LogWrite(fileLogName, ",305" + DateTime.Now.Date.ToShortDateString()); //Data END
            Arca.LogWrite(fileLogName, ",306" + DateTime.Now.Date.ToLongTimeString()); //Time END
            Arca.LogWrite(fileLogName, ",302" + testResult); //Test Result

            switch (Arca.WaitingKey())
            {
                case "ESC":
                    goto end;
                default:
                    if (testResult == "KO")
                    { goto start; }
                    break;
            }

        end:
            if (File.Exists(fileLogName))
            {
                Arca.WriteMessage(lblMessage, 46);//Invio Log al Server
                Arca.SendLog(fileLogName, SrvFileLogFolder);
            }
            CleanPanel();
        }

        private void measureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CassetteTapeLenght(true);
        }

        private void windToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void unwindToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void singleCommandToolStripMenuItem_Click(object sender, EventArgs e)
        {
            grpSingleCommand.Visible = true;
            txtTransparentCmd.Focus();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            grpSingleCommand.Visible = false;
        }

        private void btnTransparentExit_Click(object sender, EventArgs e)
        {
            grpSingleCommand.Visible = false;
        }

        private void btnTransparentSend_Click(object sender, EventArgs e)
        {
            myRs232.OpenCom();
            myRs232.SendCommand(txtTransparentCmd.Text, Convert.ToInt16(txtTransparentByte.Text),Convert.ToInt16(txtTransparentTimeO.Text));
            txtTransparentAnswer.Text = myRs232.commandAnswer;
            txtTransparentAnswer.Focus();
            myRs232.CloseCom();
        }

        private void txtTransparentCmd_TextChanged(object sender, EventArgs e)
        {
            txtTransparentAnswer.Text = "";
        }

        private void windGToolStripMenuItem_Click(object sender, EventArgs e)
        {
            myRs232.OpenCom();
            myRs232.SendCommand("DA04", 0, 100);//G
            myRs232.CloseCom();
        }

        private void windHToolStripMenuItem_Click(object sender, EventArgs e)
        {
            myRs232.OpenCom();
            myRs232.SendCommand("DB04", 0, 100);//H
            myRs232.CloseCom();
        }

        private void unwindGToolStripMenuItem_Click(object sender, EventArgs e)
        {
            myRs232.OpenCom();
            myRs232.SendCommand("DA20", 0, 100);//G
            myRs232.CloseCom();
        }

        private void unwindHToolStripMenuItem_Click(object sender, EventArgs e)
        {
            myRs232.OpenCom();
            myRs232.SendCommand("DB20", 0, 100);//H
            myRs232.CloseCom();
        }
    }
}
