!define APPNAME "CM18HC Tower Test"
!define COMPANYNAME "ARCA"
!define VERSIONMAJOR 0
!define VERSIONMINOR 1
!define VERSIONBUILD 0
!define INSTFOLDER "C:\Arca\HCTower"
!define SOURCEFOLDER "D:\Progetti VBNET\CM18tool\HCTower\bin\Debug"
OutFile "${APPNAME} V${VERSIONMAJOR}.${VERSIONMINOR}.${VERSIONBUILD}.exe"
;RequestExecutionLevel none/admin/user/highest



Section "install"
	;Banner::show /set 76 "Banner Visualizzato" /set 54 "Normal Text"
	SetOutPath "${INSTFOLDER}"
	File "${SOURCEFOLDER}\CM18HcTower.test"
	File "${SOURCEFOLDER}\CM18HcTowerCfg.ini"
	File "${SOURCEFOLDER}\HCTower.exe"
	File "${SOURCEFOLDER}\italiano.lang"
	
	CreateShortCut "$DESKTOP\${APPNAME}.lnk" "${INSTFOLDER}\HCTower.exe" ""
	
	WriteUninstaller "${INSTFOLDER}\Uninstall.exe"
SectionEnd

Section "Uninstall"
	RMDir /r "${INSTFOLDER}\*.*"
	RMDir "${INSTFOLDER}"
	Delete "$DESKTOP\${APPNAME}.lnk"
SectionEnd

Function .onInstSuccess
	#MessageBox MB_OK "Installazione eseguita correttamente!"
FunctionEnd

Function un.onUninstSuccess
	#MessageBox MB_OK "Disinstallazione eseguita correttamente!"
FunctionEnd